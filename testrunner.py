import unittest
from sys import path as syspath
from os.path import dirname as dir

syspath.append(dir(syspath[0]))
syspath.append(dir(f"{syspath[0]}/src/OneNight"))
syspath.append(dir(f"{syspath[0]}/src/Cogs"))
syspath.append(dir(f"{syspath[0]}/src/Models"))
syspath.append(dir(f"{syspath[0]}/src/Documentation"))
syspath.append(dir(f"{syspath[0]}/test/unit_tests"))
syspath.append(dir(f"{syspath[0]}/test/unit_tests/OneNight"))
syspath.append(dir(f"{syspath[0]}/test/unit_tests/Cogs"))

import unit_tests.testdbops as testdbops
import unit_tests.Cogs.testGeneralFuncs as testCogGenenralFuncs
import unit_tests.Cogs.testGameConfigFuncs as testCogGameConfigFuncs
import unit_tests.Cogs.testGameSetupFuncs as testCogGameSetupFuncs
import unit_tests.OneNight.testcore as testcore

loader = unittest.defaultTestLoader
testrunner = unittest.TextTestRunner()
suite = unittest.TestSuite()
modules = [
    testdbops,
    testCogGenenralFuncs,
    testCogGameConfigFuncs,
    testCogGameSetupFuncs,
    testcore
]
numModules = len(modules)
results = [unittest.TestResult()] * numModules
for mod in modules:
    suite.addTests(loader.loadTestsFromModule(mod))
testrunner.run(suite)