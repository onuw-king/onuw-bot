#!/bin/bash

#DOCKER ARGS
#   1. container name
#   2. image name
#   3. bot ip address
#   4. docker network
#   5. client certs file directory

#BOT ARGS
#   6. client cert file name
#   7. client key file name
#   8. ca cert file name
#   9. db api host
#   10. db api port
#   11. client nickname
#   12. self api token
#   13. db api token
#   14. bot discord token

docker container create \
--name $1 \
--ip $3 \
--network $4 \
-v $5:/app/certs \
$2 \
$6 $7 $8 $9 $10 $11 $12 $13 $14