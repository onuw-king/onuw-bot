FROM python:3.8-slim
ENV APP="/app"
ENV INSTALL="/install"
ENV LOC="/root/.local"
ENV PATH $PATH:${LOC}:${LOC}/bin

#install all dependencies
COPY ./requirements.txt ${APP}/requirements.txt
RUN pip3 install --user --no-cache-dir -r ${APP}/requirements.txt
RUN mkdir ${APP}/certs

COPY ./src/ ${APP}
COPY ./src/api_ops/ ${APP}/api_ops
COPY ./src/cogs/ ${APP}/cogs
COPY ./src/documentation/ ${APP}/documentation
COPY ./src/models/ ${APP}/models
COPY ./src/one_night/ ${APP}/one_night

WORKDIR ${APP}
EXPOSE 443
#start the service
ENTRYPOINT [ "python3", "main.py" ]