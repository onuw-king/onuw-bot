from unittest import TestCase, TestSuite
from parameterized import *
from sys import path as syspath
from os.path import dirname as dir

syspath.append(dir(syspath[0]))
syspath.append(dir(f"{syspath[0]}/src/OneNight"))
syspath.append(dir(f"{syspath[0]}/src/Cogs"))
syspath.append(dir(f"{syspath[0]}/src/Models"))

from one_night.core import *
from one_night.cards import *

class testFunctions(TestCase):

    def test_CardDictToPayload_VerifyConvertsToPayload(self):
        cardDict = {
            CardType.Villager: 3,
            CardType.Werewolf: 2
        }
        expPayload = "Villager:3;Werewolf:2"

        testPayload = CardDictToPayload(cardDict)
        self.assertTrue(expPayload == testPayload)

    def test_PayloadToCardDict_VerifyConvertsToCardDict(self):
        payload = "Villager:3;Werewolf:2"
        expCardDict = {
            CardType.Villager: 3,
            CardType.Werewolf: 2
        }

        testCardDict = PayloadToCardDict(payload)
        self.assertTrue(len(expCardDict) == len(testCardDict))
        for expK, expV in expCardDict.items():
            self.assertTrue(testCardDict[expK] == expV)

    @parameterized.expand([
        ["MustPass","Villager:1"],
        ["MustPass","Villager:1;Tanner:1"],
        ["MustPass","Villager:1;Tanner:1;Werewolf:1"]
    ])
    def test_IsValidCardPayload_VerifyRegexpPasses(self, name, payload):
        self.assertTrue(IsValidCardPayload(payload))

    @parameterized.expand([
        ["MustFail","Villager:1;"],
        ["MustFail","Villager:1;Tanner:1;"],
        ["MustFail","Villager:1;Tanner:1;Werewolf:1;"],
        ["MustFail","a:e;li:lkj;wlaw:epiogasldkjg;l89sdlfj:Ag2"],
        ["MustFail","Villager:"],
        ["MustFail","Villager:1;Tanner:"],
        ["MustFail","Villager:1;Tanner:1;Werewolf:"],
        ["MustFail","Villager:1;Tanner:;Werewolf:1"],
        ["MustFail","awoitj33oi4lkg'a['s;l;gj[3p4wk;g, 'ekseugj3o']']"]
    ])
    def test_IsValidCardPayload_VerifyRegexpFails(self, name, payload):
        self.assertFalse(IsValidCardPayload(payload))

def load_tests(loader, standard_tests, pattern):
    cases = [
        testFunctions
    ]
    suite = TestSuite()
    for case in cases:
        tests = loader.loadTestsFromTestCase(case)
        suite.addTests(tests)
    return suite

if __name__ == '__main__':
    unittest.main()