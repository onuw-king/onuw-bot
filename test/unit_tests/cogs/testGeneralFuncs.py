import asyncio
from unittest import IsolatedAsyncioTestCase, TestSuite
from os import path, remove
from sys import path as syspath
from os.path import dirname as dir

syspath.append(dir(syspath[0]))
syspath.append(dir(f"{syspath[0]}/src/OneNight"))
syspath.append(dir(f"{syspath[0]}/src/Cogs"))
syspath.append(dir(f"{syspath[0]}/src/Models"))
syspath.append(dir(f"{syspath[0]}/src/Documentation"))

from src.cogs.game import *
from src.one_night.core import *
from src.dbops import *
from src.settings import *

class testAddAdminRole(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testAddAdminRole"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyAddsRole(self):
        expRoleId = 111
        expServerId = 222
        await AddAdminRole(None, self.dbuser, expServerId, expRoleId, "")
        sql = '''
            SELECT RoleID
            FROM AdminRoles
            WHERE ServerID=%s
            AND RoleID=%s;
        '''
        data = (str(expServerId), str(expRoleId))
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            row = await curs.fetchone()
        con.close()
        self.assertTrue(expRoleId == int(row[0]))

class testRemoveAdminRole(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testRemoveAdminRole"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyRemovesRole(self):
        roleid = 1234
        serverId = 111
        sql = '''
            SELECT RoleID
            FROM AdminRoles
            WHERE RoleID=%s;
        '''
        data = (str(roleid),)
        result = ""
        await AddAdminRole(None, self.dbuser, serverId, roleid, "")
        await RemoveAdminRole(None, self.dbuser, roleid, "")
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql,data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(result is None)

def load_tests(loader, standard_tests, pattern):
    cases = [
        testAddAdminRole,
        testRemoveAdminRole
    ]
    suite = TestSuite()
    for case in cases:
        tests = loader.loadTestsFromTestCase(case)
        suite.addTests(tests)
    return suite

if __name__ == '__main__':
    unittest.main()
