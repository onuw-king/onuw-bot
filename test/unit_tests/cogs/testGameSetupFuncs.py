import asyncio
from unittest import IsolatedAsyncioTestCase, TestSuite
from os import path, remove
from parameterized import *
from sys import path as syspath
from os.path import dirname as dir

syspath.append(dir(syspath[0]))
syspath.append(dir(f"{syspath[0]}/src/OneNight"))
syspath.append(dir(f"{syspath[0]}/src/Cogs"))
syspath.append(dir(f"{syspath[0]}/src/Models"))
syspath.append(dir(f"{syspath[0]}/src/Documentation"))

from src.cogs.game import *
from src.one_night.core import *
from src.dbops import *
from src.settings import *

class testInitGame(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testInitGame"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyAddsNewGame(self):
        gmId = 1234
        chanId = 5678
        serverId = 1111
        await InitGame(None, self.dbuser, serverId, gmId, chanId)
        sql = '''
            SELECT GameMasterID
            FROM ActiveGames
            WHERE ServerID=%s
            AND GameMasterID=%s
            AND ChannelID=%s;
        '''
        data = (serverId, gmId, chanId)
        result = None
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == gmId)

class testDeleteGame(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testDeleteGame"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyDeletesGame(self):
        gmId = 1234
        chanId = 5678
        serverId = 1111
        await InitGame(None, self.dbuser, serverId, gmId, chanId)
        await DeleteGame(None, self.dbuser, serverId, gmId, chanId)
        sql = '''
            SELECT *
            FROM ActiveGames
            WHERE ServerID=%s
            AND GameMasterID=%s
            AND ChannelID=%s;
        '''
        data = (serverId, gmId, chanId)
        result = None
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(result is None)

class testAddPlayer(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testAddPlayer"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyPlayersAreAdded(self):
        serverId = 111
        gmId = 222
        chanId = 333
        userId = 444
        await InitGame(None, self.dbuser, serverId, gmId, chanId)
        await AddPlayer(None, self.dbuser, serverId, gmId, userId, "testuser")
        sql = '''
            SELECT UserID
            FROM ActivePlayers
            WHERE ServerID=%s
            AND GameMasterID=%s
            AND UserID=%s;
        '''
        data = (serverId, gmId, userId)
        result = None
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == userId)

class testRemovePlayer(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testRemovePlayer"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyPlayersAreRemoved(self):
        serverId = 111
        gmId = 222
        chanId = 333
        userId = 444
        await InitGame(None, self.dbuser, serverId, gmId, chanId)
        await AddPlayer(None, self.dbuser, serverId, gmId, userId, "testuser")
        await RemovePlayer(self.dbuser, serverId, gmId, userId)
        sql = '''
            SELECT UserID
            FROM ActivePlayers
            WHERE ServerID=%s
            AND GameMasterID=%s
            AND UserID=%s;
        '''
        data = (serverId, gmId, userId)
        result = None
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(result is None)

class testAddCardsToGame(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testAddCardsToGame"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyAddsCards(self):
        expPayload = "Villager:3;Werewolf:2;Seer:1;Tanner:1"
        serverId = 111
        gmId = 222
        chanId = 333
        await InitGame(None, self.dbuser, serverId, gmId, chanId)
        await AddCardsToGame(None, self.dbuser, expPayload, serverId, gmId)
        sql = '''
            SELECT
            Villager,
            Werewolf,
            Seer,
            Tanner
            FROM ActiveCardConfigs
            WHERE ServerID=%s
            AND GameMasterID=%s;
        '''
        data = (serverId, gmId)
        result = None
        con = await self.dbuser.GetConnection()
        #verify that the cards were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 3)
        self.assertTrue(int(result[1]) == 2)
        self.assertTrue(int(result[2]) == 1)
        self.assertTrue(int(result[3]) == 1)

    async def test_VerifyAddsAdditionalCards(self):
        initPayload = "Villager:3;Werewolf:2"
        addPayload = "Seer:1;Tanner:1"
        serverId = 111
        gmId = 222
        chanId = 333
        await InitGame(None, self.dbuser, serverId, gmId, chanId)
        await AddCardsToGame(None, self.dbuser, initPayload, serverId, gmId)
        await AddCardsToGame(None, self.dbuser, addPayload, serverId, gmId)
        sql = '''
            SELECT
            Villager,
            Werewolf,
            Seer,
            Tanner
            FROM ActiveCardConfigs
            WHERE ServerID=%s
            AND GameMasterID=%s;
        '''
        data = (str(serverId), str(gmId))
        result = None
        con = await self.dbuser.GetConnection()
        #verify that the card config settings were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 3)
        self.assertTrue(int(result[1]) == 2)
        self.assertTrue(int(result[2]) == 1)
        self.assertTrue(int(result[3]) == 1)

    async def test_VerifyWontAddCardsPastMax(self):
        initPayload = "Villager:4"
        serverId = 111
        gmId = 222
        chanId = 333
        await InitGame(None, self.dbuser, serverId, gmId, chanId)
        await AddCardsToGame(None, self.dbuser, initPayload, serverId, gmId)
        sql = '''
            SELECT
            Villager
            FROM ActiveCardConfigs
            WHERE ServerID=%s
            AND GameMasterID=%s;
        '''
        data = (str(serverId), str(gmId))
        result = None
        con = await self.dbuser.GetConnection()
        #verify that the card config settings were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 0)

    async def test_VerifyWontSequentiallyAddCardsPastMax(self):
        initPayload = "Villager:2"
        serverId = 111
        gmId = 222
        chanId = 333
        await InitGame(None, self.dbuser, serverId, gmId, chanId)
        await AddCardsToGame(None, self.dbuser, initPayload, serverId, gmId)
        await AddCardsToGame(None, self.dbuser, initPayload, serverId, gmId)
        sql = '''
            SELECT
            Villager
            FROM ActiveCardConfigs
            WHERE ServerID=%s
            AND GameMasterID=%s;
        '''
        data = (str(serverId), str(gmId))
        result = None
        con = await self.dbuser.GetConnection()
        #verify that the card config settings were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 2)

class testSubtractCardsFromGame(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testSubtractCardsFromGame"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifySubtractsCards(self):
        initPayload = "Villager:3"
        subPayload = "Villager:1"
        serverId = 111
        gmId = 222
        chanId = 333
        await InitGame(None, self.dbuser, serverId, gmId, chanId)
        await AddCardsToGame(None, self.dbuser, initPayload, serverId, gmId)
        await SubtractCardsFromGame(None, self.dbuser, subPayload, serverId, gmId)
        sql = '''
            SELECT
            Villager
            FROM ActiveCardConfigs
            WHERE ServerID=%s
            AND GameMasterID=%s;
        '''
        data = (str(serverId), str(gmId))
        result = None
        con = await self.dbuser.GetConnection()
        #verify that the card config settings were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 2)

    async def test_VerifyCompletelyRemovesCards(self):
        initPayload = "Villager:3"
        subPayload = "Villager:3"
        serverId = 111
        gmId = 222
        chanId = 333
        await InitGame(None, self.dbuser, serverId, gmId, chanId)
        await AddCardsToGame(None, self.dbuser, initPayload, serverId, gmId)
        await SubtractCardsFromGame(None, self.dbuser, subPayload, serverId, gmId)
        sql = '''
            SELECT
            Villager
            FROM ActiveCardConfigs
            WHERE ServerID=%s
            AND GameMasterID=%s;
        '''
        data = (str(serverId), str(gmId))
        result = None
        con = await self.dbuser.GetConnection()
        #verify that the card config settings were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 0)

    async def test_VerifyCantRemoveCardsIfTotalBelowZero(self):
        initPayload = "Villager:3"
        subPayload = "Villager:100"
        serverId = 111
        gmId = 222
        chanId = 333
        await InitGame(None, self.dbuser, serverId, gmId, chanId)
        await AddCardsToGame(None, self.dbuser, initPayload, serverId, gmId)
        await SubtractCardsFromGame(None, self.dbuser, subPayload, serverId, gmId)
        sql = '''
            SELECT
            Villager
            FROM ActiveCardConfigs
            WHERE ServerID=%s
            AND GameMasterID=%s;
        '''
        data = (str(serverId), str(gmId))
        result = None
        con = await self.dbuser.GetConnection()
        #verify that the card config settings were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 3)

    async def test_VerifyCantRemoveNonexistantCards(self):
        initPayload = "Villager:3"
        subPayload = "Seer:1"
        serverId = 111
        gmId = 222
        chanId = 333
        await InitGame(None, self.dbuser, serverId, gmId, chanId)
        await AddCardsToGame(None, self.dbuser, initPayload, serverId, gmId)
        await SubtractCardsFromGame(None, self.dbuser, subPayload, serverId, gmId)
        sql = '''
            SELECT
            Seer
            FROM ActiveCardConfigs
            WHERE ServerID=%s
            AND GameMasterID=%s;
        '''
        data = (str(serverId), str(gmId))
        result = None
        con = await self.dbuser.GetConnection()
        #verify that the card config settings were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 0)

class testSaveActiveGameConfig(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testSaveActiveGameConfig"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyCorrectlySavesConfig(self):
        configName = "config1"
        payload = "Villager:3;Werewolf:2;Seer:1;Robber:1"
        aid = 111
        sid = 222
        cid = 333
        expPlayerCount = 4
        expDayDur = 100
        expRoleDur = 10
        expVoteDur = 666
        await InitGame(None, self.dbuser, sid, aid, cid)
        await AddCardsToGame(None, self.dbuser, payload, sid, aid)
        await SetActiveGameConfigOption(None, self.dbuser, ConfigOption.RoleDuration, \
        expRoleDur, sid, aid)
        await SetActiveGameConfigOption(None, self.dbuser, ConfigOption.VotePhaseDuration, \
        expVoteDur, sid, aid)
        await SetActiveGameConfigOption(None, self.dbuser, ConfigOption.DayPhaseDuration, \
        expDayDur, sid, aid)
        await SaveActiveGameConfig(None, self.dbuser, sid, aid, configName)

        sql = '''
            SELECT
            Villager,
            Werewolf,
            Seer,
            Robber
            FROM CardConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (str(sid), str(aid), configName)
        result = None
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 3)
        self.assertTrue(int(result[1]) == 2)
        self.assertTrue(int(result[2]) == 1)
        self.assertTrue(int(result[3]) == 1)

        sql = '''
            SELECT
            UsesLoneWolf,
            RoleDuration,
            DayPhaseDuration,
            VotePhaseDuration,
            IsDefaultDayDuration
            FROM GameConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (str(sid), str(aid), configName)
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(bool(result[0]))
        self.assertTrue(int(result[1]) == expRoleDur)
        self.assertTrue(int(result[2]) == expDayDur)
        self.assertTrue(int(result[3]) == expVoteDur)
        self.assertTrue(bool(result[4]))

class testUseGameConfig(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testUseGameConfig"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyCorrectlyUsesConfig(self):
        configName = "config1"
        payload = "Villager:3;Werewolf:2;Seer:1;Robber:1"
        aid = 111
        sid = 222
        cid = 333
        expPlayerCount = 4
        expDayDur = 10
        expRoleDur = 100
        expVoteDur = 666
        expLoneWolf = 0
        await InitConfig(None, self.dbuser, sid, aid, configName)
        await AddCardsToConfig(None, self.dbuser, payload, sid, aid, configName)
        await SetGameConfigOption(self.dbuser, ConfigOption.UsesLoneWolf, \
        expLoneWolf, sid, aid, configName)
        await SetGameConfigOption(self.dbuser, ConfigOption.DayPhaseDuration, \
        expDayDur, sid, aid, configName)
        await SetGameConfigOption(self.dbuser, ConfigOption.RoleDuration, \
        expRoleDur, sid, aid, configName)
        await SetGameConfigOption(self.dbuser, ConfigOption.VotePhaseDuration, \
        expVoteDur, sid, aid, configName)
        await InitGame(None, self.dbuser, sid, aid, cid)
        await UseGameConfig(None, self.dbuser, sid, aid, configName)
        sql = '''
            SELECT
            Villager,
            Werewolf,
            Seer,
            Robber
            FROM ActiveCardConfigs
            WHERE ServerID=%s
            AND GameMasterID=%s;
        '''
        data = (str(sid), str(aid))
        result = None
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 3)
        self.assertTrue(int(result[1]) == 2)
        self.assertTrue(int(result[2]) == 1)
        self.assertTrue(int(result[3]) == 1)

        sql = '''
            SELECT
            UsesLoneWolf,
            RoleDuration,
            PlayerCount,
            DayPhaseDuration,
            VotePhaseDuration
            FROM ActiveGameConfigs
            WHERE ServerID=%s
            AND GameMasterID=%s;
        '''
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertFalse(bool(result[0]))
        self.assertTrue(int(result[1]) == expRoleDur)
        self.assertTrue(int(result[2]) == expPlayerCount)
        self.assertTrue(int(result[3]) == expDayDur)
        self.assertTrue(int(result[4]) == expVoteDur)

class testGetActiveGameConfigOption(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testGetActiveGameConfigOption"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    @parameterized.expand([
        [
            ConfigOption.UsesLoneWolf.name, ConfigOption.UsesLoneWolf, 0
        ],

        [
            ConfigOption.DayPhaseDuration.name, ConfigOption.DayPhaseDuration, 100
        ],

        [
            ConfigOption.RoleDuration.name, ConfigOption.RoleDuration, 200
        ],

        [
            ConfigOption.VotePhaseDuration.name, ConfigOption.VotePhaseDuration, 300
        ]
    ])
    async def test_VerifyGetsOption(self, name, option, expVal):
        sid = 111
        cid = 222
        aid = 333
        await InitGame(None, self.dbuser, sid, aid, cid)
        await SetActiveGameConfigOption(None, self.dbuser, option, expVal, sid, aid)
        testVal = await GetActiveGameConfigOption(None, self.dbuser, option, sid, aid, \
        "")
        self.assertTrue(testVal == expVal)

class testClearActiveCardConfig(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testClearActiveCardConfig"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyClearsActiveConfig(self):
        expPayload = "Villager:3;Werewolf:2;Seer:1;Tanner:1"
        serverId = 111
        gmId = 222
        chanId = 333
        await InitGame(None, self.dbuser, serverId, gmId, chanId)
        await AddCardsToGame(None, self.dbuser, expPayload, serverId, gmId)
        await ClearActiveCardConfig(self.dbuser, serverId, gmId)
        sql = '''
            SELECT Villager
            FROM ActiveCardConfigs
            WHERE ServerID=%s
            AND GameMasterID=%s;
        '''
        data = (str(serverId), str(gmId))
        result = None
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 0)

class testSetGameMaster(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testSetGameMaster"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyCascadesNewGameMaster(self):
        serverId = 111
        gmId = 222
        chanId = 333
        newgmId = 444
        await InitGame(None, self.dbuser, serverId, gmId, chanId)
        await SetGameMaster(None, self.dbuser, serverId, gmId, newgmId, "")

        sql = '''
            SELECT ActiveGames.GameMasterID
            FROM ActiveGames
            INNER JOIN ActiveGameConfigs
            ON ActiveGameConfigs.ServerID=%s
            AND ActiveGameConfigs.GameMasterID=%s
            INNER JOIN ActiveCardConfigs
            ON ActiveCardConfigs.ServerID=%s
            AND ActiveCardConfigs.GameMasterID=%s;
        '''
        data = (str(serverId), str(newgmId)) * 2
        result = None
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == newgmId)

def load_tests(loader, standard_tests, pattern):
    cases = [
        testInitGame,
        testDeleteGame,
        testAddPlayer,
        testRemovePlayer,
        testAddCardsToGame,
        testSubtractCardsFromGame,
        testSaveActiveGameConfig,
        testUseGameConfig,
        testGetActiveGameConfigOption,
        testClearActiveCardConfig,
        testSetGameMaster
    ]
    suite = TestSuite()
    for case in cases:
        tests = loader.loadTestsFromTestCase(case)
        suite.addTests(tests)
    return suite

if __name__ == '__main__':
    unittest.main()
