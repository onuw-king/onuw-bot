import asyncio
from unittest import IsolatedAsyncioTestCase, TestSuite
from os import path, remove
from parameterized import *
from sys import path as syspath
from os.path import dirname as dir

syspath.append(dir(syspath[0]))
syspath.append(dir(f"{syspath[0]}/src/OneNight"))
syspath.append(dir(f"{syspath[0]}/src/Cogs"))
syspath.append(dir(f"{syspath[0]}/src/Models"))
syspath.append(dir(f"{syspath[0]}/src/Documentation"))

from src.cogs.game import *
from src.one_night.core import *
from src.dbops import *
from src.settings import *

class testInitConfig(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testInitConfig"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyRecordIsAdded(self):
        sid = 111
        aid = 222
        gcName = "game1"
        sql = '''
            SELECT GameConfigName
            FROM GameConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (sid, aid, gcName)
        result = None
        await InitConfig(None, self.dbuser, sid, aid, gcName)
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(result[0] == gcName)

class testDeleteConfig(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testDeleteConfig"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyRecordIsDeleted(self):
        sid = 111
        aid = 222
        gcName = "game1"
        await InitConfig(None, self.dbuser, sid, aid, gcName)
        await DeleteConfig(None, self.dbuser, sid, aid, gcName)
        sql = '''
            SELECT GameConfigName
            FROM GameConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        result = None
        data = (sid, aid, gcName)
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(result is None)

class testAddCardsToConfig(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testAddCardsToConfig"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyAddsCards(self):
        expPayload = "Villager:3;Werewolf:2;Seer:1;Tanner:1"
        expCardDict = PayloadToCardDict(expPayload)
        configName = "test1"
        serverid = 111
        aid = 222
        await InitConfig(None, self.dbuser, serverid, aid, configName)
        await AddCardsToConfig(None, self.dbuser, expPayload, serverid, aid, configName)
        sql = '''
            SELECT
            Villager,
            Werewolf,
            Seer,
            Tanner
            FROM CardConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        result = None
        data = (serverid, aid, configName)
        con = await self.dbuser.GetConnection()
        #verify that the cards were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 3)
        self.assertTrue(int(result[1]) == 2)
        self.assertTrue(int(result[2]) == 1)
        self.assertTrue(int(result[3]) == 1)

    async def test_VerifyAddsAdditionalCards(self):
        initPayload = "Villager:3;Werewolf:2"
        addPayload = "Seer:1;Tanner:1"
        expPayload = initPayload + ";" + addPayload
        configName = "test1"
        serverid = 2222
        aid = 1111
        await InitConfig(None, self.dbuser, serverid, aid, configName)
        await AddCardsToConfig(None, self.dbuser, expPayload, serverid, aid, configName)
        await AddCardsToConfig(None, self.dbuser, expPayload, serverid, aid, configName)
        sql = '''
            SELECT
            Villager,
            Werewolf,
            Seer,
            Tanner
            FROM CardConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (serverid, aid, configName)
        result = None
        con = await self.dbuser.GetConnection()
        #verify that the card config settings were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 3)
        self.assertTrue(int(result[1]) == 2)
        self.assertTrue(int(result[2]) == 1)
        self.assertTrue(int(result[3]) == 1)

    async def test_VerifyWontAddCardsPastMax(self):
        initPayload = "Villager:4"
        configName = "test1"
        serverid = 2222
        aid = 1111
        await InitConfig(None, self.dbuser, serverid, aid, configName)
        await AddCardsToConfig(None, self.dbuser, initPayload, serverid, aid, configName)
        sql = '''
            SELECT
            Villager
            FROM CardConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (serverid, aid, configName)
        result = None
        con = await self.dbuser.GetConnection()
        #verify that the card config settings were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 0)

    async def test_VerifyWontSequentiallyAddCardsPastMax(self):
        initPayload = "Villager:2"
        configName = "test1"
        serverid = 2222
        aid = 1111
        await InitConfig(None, self.dbuser, serverid, aid, configName)
        await AddCardsToConfig(None, self.dbuser, initPayload, serverid, aid, configName)
        await AddCardsToConfig(None, self.dbuser, initPayload, serverid, aid, configName)
        sql = '''
            SELECT
            Villager
            FROM CardConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (serverid, aid, configName)
        result = None
        con = await self.dbuser.GetConnection()
        #verify that the card config settings were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 2)

class testSubtractCardsFromConfig(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testSubtractCardsFromConfig"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifySubtractsCards(self):
        initPayload = "Villager:3"
        subPayload = "Villager:1"
        configName = "test1"
        serverid = 2222
        aid = 1111
        await InitConfig(None, self.dbuser, serverid, aid, configName)
        await AddCardsToConfig(None, self.dbuser, initPayload, serverid, aid, configName)
        await SubtractCardsFromConfig(None, self.dbuser, subPayload, serverid, aid, \
        configName)
        sql = '''
            SELECT
            Villager
            FROM CardConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (serverid, aid, configName)
        result = None
        con = await self.dbuser.GetConnection()
        #verify that the card config settings were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 2)

    async def test_VerifyCompletelyRemovesCards(self):
        initPayload = "Villager:3"
        subPayload = "Villager:3"
        serverid = 2222
        configName = "test1"
        aid = 1111
        await InitConfig(None, self.dbuser, serverid, aid, configName)
        await AddCardsToConfig(None, self.dbuser, initPayload, serverid, aid, configName)
        await SubtractCardsFromConfig(None, self.dbuser, subPayload, serverid, aid, \
        configName)
        sql = '''
            SELECT
            Villager
            FROM CardConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (serverid, aid, configName)
        result = None
        con = await self.dbuser.GetConnection()
        #verify that the card config settings were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 0)

    async def test_VerifyCantRemoveCardsIfTotalBelowZero(self):
        initPayload = "Villager:3"
        subPayload = "Villager:100"
        serverid = 2222
        configName = "test1"
        aid = 1111
        await InitConfig(None, self.dbuser, serverid, aid, configName)
        await AddCardsToConfig(None, self.dbuser, initPayload, serverid, aid, configName)
        await SubtractCardsFromConfig(None, self.dbuser, subPayload, serverid, aid, \
        configName)
        sql = '''
            SELECT
            Villager
            FROM CardConfigs
            WHERE ServerID=%s
            AND AuthoriD=%s
            AND GameConfigName=%s;
        '''
        data = (serverid, aid, configName)
        result = None
        con = await self.dbuser.GetConnection()
        #verify that the card config settings were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 3)

    async def test_VerifyCantRemoveNonexistantCards(self):
        initPayload = "Villager:3"
        subPayload = "Seer:1"
        serverid = 2222
        configName = "test1"
        aid = 1111
        await InitConfig(None, self.dbuser, serverid, aid, configName)
        await AddCardsToConfig(None, self.dbuser, initPayload, serverid, aid, configName)
        await SubtractCardsFromConfig(None, self.dbuser, subPayload, serverid, aid, \
        configName)
        sql = '''
            SELECT
            Seer
            FROM CardConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (serverid, aid, configName)
        result = None
        con = await self.dbuser.GetConnection()
        #verify that the card config settings were correctly updated
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 0)

class testGetGameConfigOption(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testGetGameConfigOption"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    @parameterized.expand([
        [
            ConfigOption.UsesLoneWolf.name, ConfigOption.UsesLoneWolf, 0
        ],

        [
            ConfigOption.DayPhaseDuration.name, ConfigOption.DayPhaseDuration, 100
        ],

        [
            ConfigOption.RoleDuration.name, ConfigOption.RoleDuration, 200
        ],

        [
            ConfigOption.VotePhaseDuration.name, ConfigOption.VotePhaseDuration, 300
        ]
    ])
    async def test_VerifyGetsOption(self, name, option, expVal):
        sid = 111
        aid = 222
        gcName = "game1"
        await InitConfig(None, self.dbuser, sid, aid, gcName)
        await SetGameConfigOption(self.dbuser, option, expVal, sid, aid, gcName)
        testVal = await GetGameConfigOption(None, self.dbuser, option, sid, aid, gcName, "")
        self.assertTrue(testVal == expVal)

class testClearCardConfig(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testClearCardConfig"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyClearsActiveConfig(self):
        expPayload = "Villager:3;Werewolf:2;Seer:1;Tanner:1"
        serverId = 111
        aid = 222
        configName = "game1"
        await InitConfig(None, self.dbuser, serverId, aid, configName)
        await AddCardsToConfig(None, self.dbuser, expPayload, serverId, aid, configName)
        await ClearCardConfig(self.dbuser, serverId, aid, configName)
        sql = '''
            SELECT Villager
            FROM CardConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (serverId, aid, configName)
        result = None
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == 0)

class testRenameGameConfig(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testRenameGameConfig"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyChildrenUpdateOnParentUpdate(self):
        sid = 111
        aid = 222
        oldGcName = "game1"
        newGcName = "game2"
        row = GameConfigRow(sid,aid,oldGcName)
        await InsertRow(self.dbuser, row)
        await RenameGameConfig(None, self.dbuser, sid, aid, oldGcName, newGcName)
        sql = '''
            SELECT GameConfigName 
            FROM CardConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (sid, aid, newGcName)
        result = None
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(result[0] == newGcName)

    async def test_VerifyCantUpdateDuplicateName(self):
        sid = 111
        aid = 222
        oldGcName = "game1"
        newGcName = "game2"
        row = GameConfigRow(sid, aid, oldGcName)
        await InsertRow(self.dbuser, row)
        row = GameConfigRow(sid, aid, newGcName)
        await InsertRow(self.dbuser, row)
        await RenameGameConfig(None, self.dbuser, sid, aid, newGcName, oldGcName)
        sql = '''
            SELECT GameConfigName 
            FROM CardConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (sid, aid, newGcName)
        result = None
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(result[0] == newGcName)

class testCopyGameConfig(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testCopyGameConfig"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyConfigIsCopied(self):
        sid = 111
        fromAid = 222
        targetAid = 333
        fromGcName = "game1"
        targetGcName = "game2"
        cardPayload = "Villager:3;Werewolf:2"
        expPlayerCount = 2
        expLoneWolf = 0
        expRoleDur = 100
        expDayDur = 101
        expVoteDur = 102
        row = GameConfigRow(sid, fromAid, fromGcName)
        await InsertRow(self.dbuser, row)
        await AddCardsToConfig(None, self.dbuser, cardPayload, sid, fromAid, fromGcName)
        await SetGameConfigOption(self.dbuser, ConfigOption.UsesLoneWolf, expLoneWolf, \
        sid, fromAid, fromGcName)
        await SetGameConfigOption(self.dbuser, ConfigOption.DayPhaseDuration, expDayDur, \
        sid, fromAid, fromGcName)
        await SetGameConfigOption(self.dbuser, ConfigOption.RoleDuration, expRoleDur, \
        sid, fromAid, fromGcName)
        await SetGameConfigOption(self.dbuser, ConfigOption.VotePhaseDuration, \
        expVoteDur, sid, fromAid, fromGcName)

        await CopyGameConfig(None, self.dbuser, sid, fromAid, fromGcName, targetAid, \
        targetGcName, "", "")
        key = CardConfigKey(sid, targetAid, targetGcName)
        newCards = await GetCardConfig(self.dbuser, key)
        newCardPayload = CardDictToPayload(newCards)
        self.assertTrue(newCardPayload == cardPayload)
        sql = '''
            SELECT * FROM GameConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (sid, targetAid, targetGcName)
        optionsRow = None
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            optionsRow = await curs.fetchone()
        con.close()
        self.assertTrue(int(optionsRow[4]) == expRoleDur)
        self.assertTrue(bool(optionsRow[5]) == bool(expLoneWolf))
        self.assertTrue(int(optionsRow[6]) == expPlayerCount)
        self.assertTrue(int(optionsRow[7]) == expDayDur)
        self.assertTrue(int(optionsRow[8]) == expVoteDur)

def load_tests(loader, standard_tests, pattern):
    cases = [
        testInitConfig,
        testDeleteConfig,
        testAddCardsToConfig,
        testSubtractCardsFromConfig,
        testGetGameConfigOption,
        testClearCardConfig,
        testRenameGameConfig,
        testCopyGameConfig
    ]
    suite = TestSuite()
    for case in cases:
        tests = loader.loadTestsFromTestCase(case)
        suite.addTests(tests)
    return suite

if __name__ == '__main__':
    unittest.main()
