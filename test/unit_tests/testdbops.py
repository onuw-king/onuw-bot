import asyncio
from unittest import IsolatedAsyncioTestCase, TestSuite
from os import path, remove
from aiomysql import *
from parameterized import *
from sys import path as syspath
from os.path import dirname as dir

syspath.append(dir(syspath[0]))
syspath.append(dir(f"{syspath[0]}/src"))
syspath.append(dir(f"{syspath[0]}/src/OneNight"))
syspath.append(dir(f"{syspath[0]}/src/Cogs"))
syspath.append(dir(f"{syspath[0]}/src/Models"))
syspath.append(dir(f"{syspath[0]}/src/Documentation"))

from cogs.game import *
from one_night.core import *
from one_night.cards import *
from dbops import *
from models.dbmodels import *
from settings import *

class testInsertRow(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testInsertRow"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    @parameterized.expand([
        [
            "AddActiveGameRow", ActiveGameRow(111,222,333), "ActiveGames",
            [
                (0,"111"),(1,"222"),(2,"333")
            ]
        ],

        [
            "AddGameConfigRow", GameConfigRow(111,222,"game1"), "GameConfigs", 
            [
                (0,"111"),(1,"222"),(2,"game1"),(3,0),(4,30),(5,1),(6,-3),(7,-2),(8,30)
            ]
        ],
        
        [
            "AddAdminRoleRow", AdminRoleRow(111,222), "AdminRoles",
            [
                (0,"111"),(1,"222")
            ]
        ]
    ])
    async def test_VerifyRecordIsAdded(self, name, row, tablename, testDuples):
        sql = f"SELECT * FROM {tablename};"
        result = None
        await InsertRow(self.dbuser, row)
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
            result = await curs.fetchone()
        con.close()

        for i in range(0, len(testDuples)):
            self.assertTrue(result[testDuples[i][0]] == testDuples[i][1])

class testDeleteRow(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testDeleteRow"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    @parameterized.expand([
        [
            "DeleteActiveGameRow", ActiveGameRow(111,222,333), "ActiveGames",
            ActiveGameKey(111,222,333)
        ],
        
        [
            "DeleteGameConfigRow", GameConfigRow(111,222,"game1"), "GameConfigs", 
            GameConfigKey(111,222,"game1")
        ],

        [
            "DeleteAdminRoleRow", AdminRoleRow(111,222), "AdminRoles",
            AdminRoleKey(222)
        ]
    ])
    async def test_VerifyRecordIsDeleted(self, name, row, tablename, key):
        sql = f"SELECT * FROM {tablename};"
        result = None
        await InsertRow(self.dbuser, row)
        await DeleteRow(self.dbuser, key)
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(result is None)

class testGameConfigChildTables(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testGameConfigChildTables"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyGameConfigChildTablesBuilt(self):
        serverId = 111
        authorId = 222
        gConfigName = "config1"
        row = GameConfigRow(serverId, authorId, gConfigName)
        result = None
        sql = '''
            SELECT *
            FROM CardConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (str(serverId), str(authorId), str(gConfigName))
        await InsertRow(self.dbuser, row)
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql,data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == serverId)
        self.assertTrue(int(result[1]) == authorId)
        self.assertTrue(result[2] == gConfigName)
        for rowItem in result[3:]:
            self.assertTrue(int(rowItem) == 0)

    async def test_VerifyGameConfigOptsUpdate(self):
        sid = 111
        aid = 222
        gcName = "config1"
        expPlayerCount = 5
        expGameDur = expPlayerCount + 1
        sqlCards = '''
            UPDATE CardConfigs
            SET
            Villager=3,
            Werewolf=2,
            Seer=1,
            Tanner=1,
            Hunter=1
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        sqlConfigs = '''
            SELECT PlayerCount,
            DayPhaseDuration
            FROM GameConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (str(sid), str(aid), gcName)
        result = None
        row = GameConfigRow(sid, aid, gcName)
        key = GameConfigKey(sid, aid, gcName)
        await InsertRow(self.dbuser, row)
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sqlCards, data)
        await con.commit()
        async with con.cursor() as curs:
            await curs.execute(sqlConfigs, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(int(result[0]) == expPlayerCount)
        self.assertTrue(int(result[1]) == expGameDur)

    async def test_VerifyGameConfigChildTablesDeleteOnParentRecordDelete(self):
        sid = 111
        aid = 222
        gcName = "game1"
        sql = '''
            SELECT * FROM CardConfigs
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (str(sid), str(aid), gcName)
        row = GameConfigRow(sid, aid, gcName)
        key = GameConfigKey(sid, aid, gcName)
        result = None
        await InsertRow(self.dbuser, row)
        await DeleteRow(self.dbuser, key)
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(result is None)

class testActiveGameConfigChildTables(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testActiveGameConfigChildTables"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyActiveGameChildTablesBuilt(self):
        serverId = 111
        chanId = 222
        gmId = 333
        expUserId = gmId
        isDefaultDayDur = 1
        expLoneWolf = 1
        expRoleDur = 30
        expPlayerCount = -3
        expVoteDur = 30
        expGameDur = expPlayerCount + 1
        row = ActiveGameRow(serverId, gmId, chanId)
        sqlPlayer = '''
            SELECT *
            FROM ActivePlayers
            WHERE GameMasterID=%s;
        '''
        sqlGConfig = '''
            SELECT *
            FROM ActiveGameConfigs
            WHERE GameMasterID=%s;
        '''
        sqlCConfig = '''
            SELECT *
            FROM ActiveCardConfigs
            WHERE GameMasterID=%s;
        '''
        data = (str(gmId),)
        playerRow = None
        gConfigRow = None
        cConfigRow = None
        await InsertRow(self.dbuser, row)
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sqlPlayer, data)
            playerRow = await curs.fetchone()
            await curs.execute(sqlGConfig, data)
            gConfigRow = await curs.fetchone()
            await curs.execute(sqlCConfig, data)
            cConfigRow = await curs.fetchone()
        con.close()
        #default active player row
        self.assertTrue(int(playerRow[0]) == serverId)
        self.assertTrue(int(playerRow[1]) == gmId)
        self.assertTrue(int(playerRow[2]) == gmId)
        #default active game config row
        self.assertTrue(int(gConfigRow[0]) == serverId)
        self.assertTrue(int(gConfigRow[1]) == gmId)
        self.assertTrue(int(gConfigRow[2]) == expPlayerCount)
        self.assertTrue(int(gConfigRow[3]) == isDefaultDayDur)
        self.assertTrue(int(gConfigRow[4]) == expLoneWolf)
        self.assertTrue(int(gConfigRow[5]) == expRoleDur)
        self.assertTrue(int(gConfigRow[6]) == expGameDur)
        self.assertTrue(int(gConfigRow[7]) == expVoteDur)
        #default active card config info
        self.assertTrue(int(cConfigRow[0]) == serverId)
        self.assertTrue(int(cConfigRow[1]) == gmId)
        for rowItem in cConfigRow[2:]:
            self.assertTrue(int(rowItem) == 0)

    async def test_VerifyActiveGameConfigOptsUpdate(self):
        serverId = 111
        chanId = 222
        gmId = 333
        expPlayerCount = 5
        expGameDur = expPlayerCount + 1
        sqlCard = '''
            UPDATE ActiveCardConfigs
            SET Villager=3,
            Werewolf=2,
            Seer=1,
            Tanner=1,
            Hunter=1
            WHERE GameMasterID=%s;
        '''
        sqlGame = '''
            SELECT PlayerCount,
            DayPhaseDuration
            FROM ActiveGameConfigs
            WHERE GameMasterID=%s;
        '''
        data = (str(gmId),)
        result = None
        row = ActiveGameRow(serverId, gmId, chanId)
        await InsertRow(self.dbuser, row)
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sqlCard, data)
        await con.commit()
        async with con.cursor() as curs:
            await curs.execute(sqlGame, data)
            result = await curs.fetchone()
        con.close()
        self.assertTrue(result[0] == expPlayerCount)
        self.assertTrue(result[1] == expGameDur)

    async def test_VerifyActiveGameChildTablesDeleteOnParentRecordDelete(self):
        sid = 111
        cid = 222
        gmid = 333
        data = (str(sid), str(gmid))
        sqlPlayers = '''
            SELECT * FROM ActivePlayers
            WHERE ServerID=%s
            AND GameMasterID=%s;
        '''
        sqlGConfig = '''
            SELECT * FROM ActiveGameConfigs
            WHERE ServerID=%s
            AND GameMasterID=%s;
        '''
        sqlCConfig = '''
            SELECT * FROM ActiveCardConfigs
            WHERE ServerID=%s
            AND GameMasterID=%s;
        '''
        playerRow = ""
        gConfigRow = ""
        cConfigRow = ""
        row = ActiveGameRow(sid, gmid, cid)
        key = ActiveGameKey(sid, gmid, cid)
        await InsertRow(self.dbuser, row)
        await DeleteRow(self.dbuser, key)
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sqlPlayers, data)
            playerRow = await curs.fetchone()
            await curs.execute(sqlGConfig, data)
            gConfigRow = await curs.fetchone()
            await curs.execute(sqlCConfig, data)
            cConfigRow = await curs.fetchone()
        con.close()
        self.assertTrue(playerRow is None)
        self.assertTrue(gConfigRow is None)
        self.assertTrue(cConfigRow is None)

class testGMsGameExistsInChannel(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testGMsGameExistsInChannel"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyReturnsTrue(self):
        expGmid = 111
        expCid = 222
        expSid = 333
        row = ActiveGameRow(expSid, expGmid, expCid)
        key = ActiveGameKey(expSid, expGmid, expCid)
        await InsertRow(self.dbuser, row)
        doesExist = await GMsGameExistsInChannel(self.dbuser, key)
        self.assertTrue(doesExist)

    async def test_VerifyReturnsFalse(self):
        expGmid = 111
        expCid = 222
        expSid = 333
        row = ActiveGameRow(expSid, expGmid, expCid)
        key = ActiveGameKey(expSid, expGmid, 666)
        await InsertRow(self.dbuser, row)
        doesExist = await GMsGameExistsInChannel(self.dbuser, key)
        self.assertFalse(doesExist)

class testGetPlayers(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testGetPlayers"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyReturnsNothing(self):
        key = ActivePlayerKey(1,2,3)
        players = await GetPlayers(self.dbuser, key, None)
        self.assertTrue(players == {})

    async def test_VerifyReturnsPlayer(self):
        sid = 222
        gmid = 333
        chanId = 111
        expUid = gmid
        name = "player1"
        expPlayer = Player(expUid, None)
        row = ActiveGameRow(sid, gmid, chanId)
        key = ActivePlayerKey(sid, gmid, expUid)
        await InsertRow(self.dbuser, row)
        players = await GetPlayers(self.dbuser, key, None)
        self.assertTrue(expUid in players)
        self.assertTrue(players[expUid] == expPlayer)

class testGetCardConfig(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testGetCardConfig"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyReturnsNothing(self):
        key = CardConfigKey(1, 2, 3)
        cardConfig = await GetCardConfig(self.dbuser, key)
        self.assertTrue(cardConfig == {})

    async def test_VerifyReturnsConfig(self):
        gameConfigName = "game1"
        sid = 111
        authorId = 222
        expCardConfig = {
            CardType.Villager: 3,
            CardType.Robber: 1
        }
        sql = '''
            UPDATE CardConfigs
            SET
            Villager=3,
            Robber=1
            WHERE ServerID=%s
            AND AuthorID=%s
            AND GameConfigName=%s;
        '''
        data = (str(sid), str(authorId), gameConfigName)
        row = GameConfigRow(sid, authorId, gameConfigName)
        key = CardConfigKey(sid, authorId, gameConfigName)
        await InsertRow(self.dbuser, row)
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
        await con.commit()
        con.close()
        cardConfig = await GetCardConfig(self.dbuser, key)
        self.assertTrue(len(expCardConfig) == len(cardConfig))
        for expCType, expNumCards in expCardConfig.items():
            self.assertTrue(expCType in cardConfig)
            self.assertTrue(cardConfig[expCType] == expNumCards)

class testGetActiveCardConfig(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testGetActiveCardConfig"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyReturnsNothing(self):
        key = ActiveCardConfigKey(1, 2)
        cardConfig = await GetActiveCardConfig(self.dbuser, key)
        self.assertTrue(cardConfig == {})

    async def test_VerifyReturnsConfig(self):
        gmId = 333
        sid = 111
        chanId = 222
        expCardConfig = {
            CardType.Villager: 3,
            CardType.Robber: 1
        }
        sql = '''
            UPDATE ActiveCardConfigs
            SET
            Villager=3,
            Robber=1
            WHERE ServerID=%s
            AND GameMasterID=%s;
        '''
        data = (str(sid), str(gmId))
        row = ActiveGameRow(sid, gmId, chanId)
        key = ActiveCardConfigKey(sid, gmId)
        await InsertRow(self.dbuser, row)
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql, data)
        await con.commit()
        con.close()
        cardConfig = await GetActiveCardConfig(self.dbuser, key)
        self.assertTrue(len(expCardConfig) == len(cardConfig))
        for expCType, expNumCards in expCardConfig.items():
            self.assertTrue(expCType in cardConfig)
            self.assertTrue(cardConfig[expCType] == expNumCards)

class testGameConfigExists(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testGameConfigExists"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyReturnsTrue(self):
        gConfigName = "gameConfig1"
        sid = 111
        authorId = 222
        row = GameConfigRow(sid, authorId, gConfigName)
        key = GameConfigKey(sid, authorId, gConfigName)
        await InsertRow(self.dbuser, row)
        doesExist = await GameConfigExists(self.dbuser, key)
        self.assertTrue(doesExist)

    async def test_VerifyReturnsFalse(self):
        key = GameConfigKey(1, 2, "helloworld")
        doesExist = await GameConfigExists(self.dbuser, key)
        self.assertFalse(doesExist)

class testActiveGameConfigExists(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testActiveGameConfigExists"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyReturnsTrue(self):
        gmId = 222
        sid = 111
        chanId = 333
        row = ActiveGameRow(sid, gmId, chanId)
        key = ActiveGameConfigKey(sid, gmId)
        await InsertRow(self.dbuser, row)
        doesExist = await ActiveGameConfigExists(self.dbuser, key)
        self.assertTrue(doesExist)

    async def test_VerifyReturnsFalse(self):
        key = ActiveGameConfigKey(1, 2)
        doesExist = await ActiveGameConfigExists(self.dbuser, key)
        self.assertFalse(doesExist)

### UNCOMMENT AFTER UPDATING:
###     -InitGame()
###     -AddCardsToGame()
###     -SetActiveGameConfigOption
class testGetGame(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testGetGame"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyGameIsCorrectlyBuilt(self):
        payload = "Villager:3;Werewolf:2;Seer:1;Tanner:1"
        expPlayerCount = 4
        expRoleDur = 50
        expGameDur = 10
        expLoneWolf = 0
        serverId = "111"
        gmId = "222"
        chanId = "333"
        gamekey = ActiveGameKey(serverId, gmId, chanId)
        await InitGame(None, self.dbuser, serverId, gmId, chanId)
        await AddCardsToGame(None, self.dbuser, payload, serverId, gmId)
        await SetActiveGameConfigOption(None, self.dbuser, ConfigOption.UsesLoneWolf, expLoneWolf, serverId, gmId)
        await SetActiveGameConfigOption(None, self.dbuser, ConfigOption.DayPhaseDuration, expGameDur, serverId, gmId)
        await SetActiveGameConfigOption(None, self.dbuser, ConfigOption.RoleDuration, expRoleDur, serverId, gmId)
        game = await GetGame(self.dbuser, gamekey, None)

        self.assertFalse(game.Config.HasLoneWolf)
        self.assertTrue(game.Config.DayPhaseDuration == expGameDur)
        self.assertTrue(game.Config.RoleDuration == expRoleDur)

        numVillagers = 0
        numWWs = 0
        numSeers = 0
        numTanners = 0
        for card in game.Deck:
            if isinstance(card, Seer):
                numSeers += 1
            elif isinstance(card, Tanner):
                numTanners += 1
            elif isinstance(card, Villager):
                numVillagers += 1
            elif isinstance(card, Werewolf):
                numWWs += 1
        self.assertTrue(numVillagers == 3)
        self.assertTrue(numWWs == 2)
        self.assertTrue(numSeers == 1)
        self.assertTrue(numTanners == 1)

class testIsTheirConfig(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testIsTheirConfig"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyReturnsFalse(self):
        gameConfigName = "game1"
        sid = 111
        authorId = 222
        row = GameConfigRow(sid, authorId, gameConfigName)
        key = GameConfigKey(sid, 333, gameConfigName)
        await InsertRow(self.dbuser, row)
        isTheirConfig = await IsTheirConfig(self.dbuser, key)
        self.assertFalse(isTheirConfig)

    async def test_VerifyReturnsTrue(self):
        gameConfigName = "game1"
        sid = 111
        authorId = 222
        row = GameConfigRow(sid, authorId, gameConfigName)
        key = GameConfigKey(sid, authorId, gameConfigName)
        await InsertRow(self.dbuser, row)
        isTheirConfig = await IsTheirConfig(self.dbuser, key)
        self.assertTrue(isTheirConfig)

class testActiveGameExistsInChannel(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        loop = asyncio.get_event_loop()
        self.dbname = "testActiveGameExistsInChannel"
        self.dbuser = DatabaseUser("onuwtest", "localhost", "test", loop)
        await InitDatabase(self.dbuser, self.dbname)
        self.dbuser.dbname = self.dbname
        await InitTables(self.dbuser)

    async def asyncTearDown(self):
        sql = f"DROP DATABASE {self.dbname};"
        con = await self.dbuser.GetConnection()
        async with con.cursor() as curs:
            await curs.execute(sql)
        await con.commit()
        con.close()

    async def test_VerifyReturnsTrue(self):
        expGmid = 111
        expCid = 222
        expSid = 333
        row = ActiveGameRow(expSid, expGmid, expCid)
        await InsertRow(self.dbuser, row)
        doesExist = await ActiveGameExistsInChannel(self.dbuser, expCid)
        self.assertTrue(doesExist)

    async def test_VerifyReturnsFalse(self):
        expGmid = 111
        expCid = 222
        expSid = 333
        row = ActiveGameRow(expSid, expGmid, expCid)
        await InsertRow(self.dbuser, row)
        doesExist = await ActiveGameExistsInChannel(self.dbuser, 1234)
        self.assertFalse(doesExist)

def load_tests(loader, standard_tests, pattern):
    cases = [
        testInsertRow,
        testDeleteRow,
        testGameConfigChildTables,
        testActiveGameConfigChildTables,
        testGMsGameExistsInChannel,
        testGetPlayers,
        testGetCardConfig,
        testGetActiveCardConfig,
        testGameConfigExists,
        testActiveGameConfigExists,
        testGetGame,
        testIsTheirConfig,
        testActiveGameExistsInChannel
    ]
    suite = TestSuite()
    for case in cases:
        tests = loader.loadTestsFromTestCase(case)
        suite.addTests(tests)
    return suite

if __name__ == '__main__':
    unittest.main()
