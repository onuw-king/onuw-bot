import documentation.disclaimers as disclaimers

initconfig = lambda prefix, cmdname: f'''
Initializes a named, blank game configuration with default option values.

Prerequisites:
    1. {disclaimers.noactivegame}

Usage:
    {prefix}{cmdname} MyConfigName
'''
deleteconfig = lambda prefix, cmdname: f'''
Deletes one of your game configs by name.

Prerequisites:
    1. {disclaimers.noactivegame}

    2. {disclaimers.ownconfig}
    
Usage:
    {prefix}{cmdname} MyConfigName
'''
forcedeleteconfig = lambda prefix, cmdname: f'''
Deletes a game config, given the config name and the user who created the config.

Prerequisites:
    1. {disclaimers.adminroles}
    
Usage:
    {prefix}{cmdname} ConfigName @user
'''
viewconfigs = lambda prefix, cmdname: f'''
View all game configs on the server. Additionally, the number of players may be used as an input to find game configs by number of players.

Prerequisites:
    1. {disclaimers.noactivegame}

Usages:
    {prefix}{cmdname}
    {prefix}{cmdname} NumberOfPlayers
'''
configaddcards = lambda prefix, cmdname: f'''
Adds the specified cards to the specified game config. Multiple cards and card types may be added in a single command.

Prerequisites:
    1. {disclaimers.noactivegame}

Usage:
    {prefix}{cmdname} MyConfigName Card1:AmountOfCard1;Card2:AmountOfCard2

Example usage:
    {prefix}{cmdname} MyConfigName Villager:2;Seer:1
'''
configsubtractcards = lambda prefix, cmdname: f'''
Subtracts the specified cards to the specified game config. Multiple cards and card types may be subtracted in a single command.

Prerequisites:
    1. {disclaimers.noactivegame}

Usage:
    {prefix}{cmdname} MyConfigName Card1:AmountOfCard1;Card2:AmountOfCard2

Example usage:
    {prefix}{cmdname} MyConfigName Villager:2;Seer:1
'''
viewconfigcards = lambda prefix, cmdname: f'''
View the cards of your game config. Supplying a user searches for the cards of that user's game config.

Prerequisites:
    1. {disclaimers.noactivegame}

Usage:
    {prefix}{cmdname} ConfigName
    {prefix}{cmdname} ConfigName @user
'''
configlonewolf = lambda prefix, cmdname: f'''
Gets the current value of the Lone Wolf option of your supplied game config.
If a value is supplied after the game config name, then the command sets the Lone Wolf option of your supplied game config. The input must be either 1 or 0.

If Lone Wolf is set to 1, then a werewolf, if they are the only werewolf in the game, may look at a middle card.

Prerequisites:
    1. {disclaimers.noactivegame}

Usages:
    {prefix}{cmdname} GameConfigName
    {prefix}{cmdname} GameConfigName 1or0
'''
configdayduration = lambda prefix, cmdname: f'''
Gets the current value of the Day Duration option of your supplied game config.
If a value is supplied after the game config name, then the command sets the duration for the day phase of a game for your game config. The unit of the input is minutes.

Prerequisites:
    1. {disclaimers.noactivegame}

Usages:
    {prefix}{cmdname} GameConfigName
    {prefix}{cmdname} GameConfigName NumberOfMinutes
'''
configroleduration = lambda prefix, cmdname: f'''
Gets the current value of the Role Duration option of your supplied game config.
If a value is supplied after the game config name, then the command sets the amount of time a player has to do their night action during a game for your game config. The unit of the input is seconds.

Prerequisites:
    1. {disclaimers.noactivegame}

Usages:
    {prefix}{cmdname} GameConfigName
    {prefix}{cmdname} GameConfigName NumberOfSeconds
'''
configvoteduration = lambda prefix, cmdname: f'''
Gets the current value of the Vote Duration option of your supplied game config.
If a value is supplied after the game config name, then the command sets the amount of time a player has to vote during a game for your game config. The unit of the input is seconds.

Prerequisites:
    1. {disclaimers.noactivegame}

Usages:
    {prefix}{cmdname} GameConfigName
    {prefix}{cmdname} GameConfigName NumberOfSeconds
'''
renameconfig = lambda prefix, cmdname: f'''
Renames one of your game configs.

Prerequisites:
    1. {disclaimers.noactivegame}

Usage:
    {prefix}{cmdname} OldConfigName NewConfigName
'''
copyconfig = lambda prefix, cmdname: f'''
Copies another user's game config to your collection of game configs.

Prerequisites:
    1. {disclaimers.noactivegame}

Usage:
    {prefix}{cmdname} NewConfigName @FromUser FromConfigName

Note:
    FromUser = the user you want to copy from.
    FromConfigName = the name of the game config that you want to copy.
'''