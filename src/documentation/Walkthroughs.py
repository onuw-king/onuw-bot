import documentation.disclaimers as disclaimers
import documentation.extra as extra

configsetup = lambda prefix, initconfigcmdname, addcardscmdname: f'''
Basic configuration setup:

Steps:
    1. Create a named, blank configuration using the {prefix}{initconfigcmdname} command.

    2. Add cards to the configuration using the {prefix}{addcardscmdname} command.

Note: {extra.info_commands(prefix)}
'''
gamesetup = lambda prefix, initgamecmdname, addcardscmdname, addplayerscmdname: f'''
Basic game setup:

Steps:
    1. Initialize a game with a blank configuration using the {prefix}{initgamecmdname} command.

    2. Add cards to the configuration using the {prefix}{addcardscmdname} command.

    3. Add players to the game using the {prefix}{addplayerscmdname} command.
       Note: A game must have 3 to 10 players.

Note: {extra.info_commands(prefix)}
'''
basicgameworkflow = lambda prefix, pregamecmdname, startcmdname, nacmdname, votecmdname: f'''
Basic game workflow:

Prerequisites:
    1. Must be the game master for the game in the channel.

    2. Must have 3-10 players added to the game.

    3. Must have enough cards for the amount of players. (Cards needed = Number of players + 3)

Steps:
    1. Start the pregame phase of the game by using the {prefix}{pregamecmdname} command.

    2. Once ready, start the game by using the {prefix}{startcmdname} command.

    3. If you have a card that has a night action that requires you to do something, then 
       you will need to direct message the bot the {prefix}{nacmdname} command to perform
       your night action. The bot will direct message you all of the choices that you can` make.

    4. Once the night and day phases are over, then you will need to direct message the bot the
       {prefix}{votecmdname} command to vote for the person you want to kill. The bot will direct
       message you all of the choices that you can make.

    5. Once voting has finished, the bot will print the outcome of the game in the chat.

Note: {extra.info_commands(prefix)}
'''
availableroles = lambda roleList: f'''
The following is a list of available roles.

{roleList}
'''