import documentation.disclaimers as disclaimers

initgame = lambda prefix, cmdname: f'''
Initializes a game with a blank game configuration in the channel the command was called. The person who calls the command is the game master.

Usage:
    {prefix}{cmdname}
'''
disbandgame = lambda prefix, cmdname: f'''
Disbands the your game in the channel where the command was called.

Prerequisites:
    1. {disclaimers.gamemaster}

Usage:
    {prefix}{cmdname}
'''
forcedisbandgame = lambda prefix, cmdname: f'''
Disbands a user's game, given the channel and the user name.

Prerequisites:
    1. {disclaimers.adminroles}

    2. {disclaimers.gameinsession}

Usage:
    {prefix}{cmdname} #channel @user
'''
viewgames = lambda prefix, cmdname: f'''
View all active games in the server.

Prerequisites:
    1. {disclaimers.noactivegame}

Usage:
    {prefix}{cmdname}
'''
addplayers = lambda prefix, cmdname: f'''
Add players to the game.

Prerequisites:
    1. {disclaimers.gamemaster}

Usage:
    {prefix}{cmdname} @player1 @player2 @playerN
'''
removeplayers = lambda prefix, cmdname: f'''
Remove players from the game.

Prerequisites:
    1. {disclaimers.gamemaster}

Usage:
    {prefix}{cmdname} @player1 @player2 @playerN
'''
viewplayers = lambda prefix, cmdname: f'''
View all players in the game.

Prerequisites:
    1. {disclaimers.gamemaster}

Usage:
    {prefix}{cmdname}
'''
addcards = lambda prefix, cmdname: f'''
Adds the specified cards to the game. Multiple cards and card types may be added in a single command.

Prerequisites:
    1. {disclaimers.gamemaster}

Usage:
    {prefix}{cmdname} Card1:AmountOfCard1;Card2:AmountOfCard2

Example usage:
    {prefix}{cmdname} Villager:2;Seer:1
'''
subtractcards = lambda prefix, cmdname: f'''
Adds the specified cards to the game. Multiple cards and card types may be added in a single command.

Prerequisites:
    1. {disclaimers.gamemaster}

Usage:
    {prefix}{cmdname} Card1:AmountOfCard1;Card2:AmountOfCard2

Example usage:
    {prefix}{cmdname} Villager:2;Seer:1
'''
viewcards = lambda prefix, cmdname: f'''
View the cards in the game.

Prerequisites:
    1. {disclaimers.gamemaster}

Usage:
    {prefix}{cmdname}
'''
saveconfig = lambda prefix, cmdname: f'''
Saves the current game config for later usage.

Prerequisites:
    1. {disclaimers.gamemaster}

Usage:
    {prefix}{cmdname} ConfigName
'''
useconfig = lambda prefix, cmdname: f'''
Uses a saved config for the game.

Prerequisites:
    1. {disclaimers.gamemaster}

Usage:
    {prefix}{cmdname} ConfigName
'''
lonewolf = lambda prefix, cmdname: f'''
Gets the current value of the Lone Wolf option of your game's config.
If a value is supplied, then the command sets the Lone Wolf option of your game's config. The input must be either 1 or 0.

If Lone Wolf is set to 1, then a werewolf, if they are the only werewolf in the game, may look at a middle card.

Prerequisites:
    1. {disclaimers.gamemaster}

Usages:
    {prefix}{cmdname}
    {prefix}{cmdname} 1or0
'''
dayduration = lambda prefix, cmdname: f'''
Gets the current value of the Day Duration option of your game's config.
If a value is supplied, then the command sets the duration for the day phase of your game's config. The unit of the input is minutes.

Prerequisites:
    1. {disclaimers.gamemaster}

Usages:
    {prefix}{cmdname}
    {prefix}{cmdname} NumberOfMinutes
'''
roleduration = lambda prefix, cmdname: f'''
Gets the current value of the Role Duration option of your game's config.
If a value is supplied, then the command sets the amount of time a player has to do their night action during a game for your game's config. The unit of the input is seconds.

Prerequisites:
    1. {disclaimers.gamemaster}

Usages:
    {prefix}{cmdname}
    {prefix}{cmdname} NumberOfSeconds
'''
voteduration = lambda prefix, cmdname: f'''
Gets the current value of the Vote Duration option of your game's config.
If a value is supplied, then the command sets the amount of time a player has to vote during a game for your game's config. The unit of the input is seconds.

Prerequisites:
    1. {disclaimers.gamemaster}

Usages:
    {prefix}{cmdname}
    {prefix}{cmdname} NumberOfSeconds
'''
givegamemaster = lambda prefix, cmdname: f'''
Gives the Game Master status to another player.

Prerequisites:
    1. {disclaimers.gamemaster}

Usage:
    {prefix}{cmdname} @Player
'''