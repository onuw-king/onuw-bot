wakeorder = lambda prefix: f'''
**General Wake Order:**

1st. Doppelganger
2nd. Werewolf
3rd. Minion
4th. Mason
5th. Seer
6th. Robber
7th. Troublemaker
8th. Drunk
9th. Insomniac

Use the {prefix}start command to begin.
'''

doppelganger = '''
**Doppelganger Summary:**:

Looks at (but does not switch) one other player's card and becomes that role.

As a Villager, Tanner, or Hunter, performs no other action at night.
As a Werewolf or Mason, they wake up again with that role.
As a Seer, Robber, Troublemaker, or Drunk, they immediately perform that role's action and does not wake up again later.
As a Minion, learns who the Werewolves are.
As Insomniac, wakes up at the end of the night to see their role.
'''

werewolf = '''
**Werewolf Summary:**

Wakes up and sees other Werewolves. When the Lone Wolf Option is on (it is on by default), if there is only one Werewolf, may look at a center card.
'''

minion = '''
**Minion Summary:**

Learns who the Werewolves are, if any.
'''

mason = '''
**Mason Summary:**

Learns who the other Mason is, if any.
'''

seer = '''
**Seer Summary:**

May look at one other player's card or two of the center cards.
'''

robber = '''
**Robber Summary:**

May swap their role with another player, then look at their new role.
'''

troublemaker = '''
**Troublemaker Summary:**

May swap two other roles without looking at them.
'''

drunk = '''
**Drunk Summary:**

Must exchange his Drunk card with any of the center cards, but does not look at their new role.
'''

insomniac = '''
**Insomniac Summary:**

Wakes up last and looks at their role to see if it has changed.
'''

villager = '''
**Villager Summary:**

Doesn't wake, and has no abilities.
'''

tanner = '''
**Tanner Summary:**

Doesn't wake. On their own team, only wins if they die. If they die, Werewolf team can't win.
'''

hunter = '''
**Hunter Summary:**

Doesn't Wake. If they die, whoever they vote for also dies.
'''