import documentation.disclaimers as disclaimers

addadminroles = lambda prefix, cmdname: f'''
Adds server roles to the bot as admin roles so that people with those roles can use commands that require being an admin.

Prerequisites:
    1. {disclaimers.adminroles}

Usage:
    {prefix}{cmdname} role1 role2 roleN
'''
deleteadminroles = lambda prefix, cmdname: f'''
Deletes server roles from the bot, which means that they are no longer admin roles.

Prerequisites:
    1. {disclaimers.adminroles}

Usage:
    {prefix}{cmdname} roleid1 roleid2 roleidN
'''
viewadminroles = lambda prefix, cmdname: f'''
Shows all of the server roles that are saved as admin roles in the bot.

Prerequisites:
    1. {disclaimers.adminroles}

Usage:
    {prefix}{cmdname}
'''