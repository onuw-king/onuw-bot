import documentation.RoleSummaries as rolesums
from one_night.cards import *

na_ingame = lambda prefix, cmdname: f"Perform your night action by using the {prefix}{cmdname} command."
na_roleinfo = {
    Doppelganger: rolesums.doppelganger,
    Werewolf: rolesums.werewolf,
    Minion: rolesums.minion,
    Mason: rolesums.mason,
    Seer: rolesums.seer,
    Robber: rolesums.robber,
    Troublemaker: rolesums.troublemaker,
    Drunk: rolesums.drunk,
    Insomniac: rolesums.insomniac,
    Villager: rolesums.villager,
    Tanner: rolesums.tanner,
    Hunter: rolesums.hunter
}