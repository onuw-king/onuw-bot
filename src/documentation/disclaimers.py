adminroles = "Only users who have a server role that has admin permissions or is flagged as a game admin role may use this command."
noactivegame = "There must not be an active game in the channel for which you are not the game master."
gamemaster = "Must be called by a player who is the channel's game master."
dmonly = "You must direct message the bot this command."
ownconfig = "You must have created the game configuration that you want to delete."
gameinsession = "There must not be a game in session."