import documentation.disclaimers as disclaimers

deal = lambda prefix, cmdname: f'''
Starts the dealing phase of a game, which tells players their cards and provides additional game information.

Prerequisites:
    1. {disclaimers.gamemaster}

Usage:
    {prefix}{cmdname}
'''
start = lambda prefix, cmdname: f'''
If a game is in the pregame phase, it begins the game.

Prerequisites:
    1. {disclaimers.gamemaster}

Usage:
    {prefix}{cmdname}
'''
abort = lambda prefix, cmdname: f'''
Aborts the game at any point.

Prerequisites:
    1. {disclaimers.gamemaster}

Usage:
    {prefix}{cmdname}
'''
na = lambda prefix, cmdname: f'''
Does the night action corresponding to your role during a game, when you are prompted by the bot.

Prerequisites:
    1. {disclaimers.dmonly}

Usage:
    {prefix}{cmdname} Input1 Input2 InputN
'''
vote = lambda prefix, cmdname: f'''
Votes for a player during the voting phase of a game.

Prerequisites:
    1. {disclaimers.dmonly}
    
Usage:
    {prefix}{cmdname} Input
'''