from enum import Enum

from models.db import (
    DatabaseTable,
    dbdata_factory
)

class RequestType(Enum):
    get = 0
    add = 1
    update = 2
    delete = 3

class ResponseType(Enum):
    info = 0
    error = 1

class Request:
    def __init__(self, request_type: RequestType, table: DatabaseTable):
        self.request_type = request_type
        self.table = table

    def dict(self):
        return {
            "type": self.request_type.name,
            "table": self.table.name
        }
    
    def __eq__(self, other):
        raise NotImplementedError(f"__eq__ not implemented for {type(self).__name__}")

    def __ne__(self, other):
        return not(self.__eq__(other))

class AddRequest(Request):
    def __init__(self, table: DatabaseTable, row: dict):
        super().__init__(RequestType.add, table)
        self.data = dbdata_factory(row, table)

    def dict(self):
        parent = super().dict()
        return {
            **parent,
            "data": self.data.dict()
        }

    def __eq__(self, other):
        if not(isinstance(other, AddRequest)):
            return False
        if not(self.request_type == other.request_type):
            return False
        if not(self.table == other.table):
            return False
        return self.data == other.data

class UpdateRequest(Request):
    def __init__(self, table: DatabaseTable, row: dict, index: dict):
        super().__init__(RequestType.update, table)
        self.data = dbdata_factory(row, table)
        self.index = index

    def dict(self):
        parent = super().dict()
        return {
            **parent,
            "data": self.data.dict(),
            "index": self.index
        }

    def __eq__(self, other):
        if not(isinstance(other, UpdateRequest)):
            return False
        if not(self.request_type == other.request_type):
            return False
        if not(self.table == other.table):
            return False
        if not(self.data == other.data):
            return False
        if not(len(self.index) == len(other.index)):
            return False
        for exp_key, exp_val in self.index.items():
            if not(other.index[exp_key] == exp_val):
                return False
        return True

class GetRequest(Request):
    def __init__(self, table: DatabaseTable, index: dict):
        super().__init__(RequestType.get, table)
        self.index = index

    def dict(self):
        parent = super().dict()
        return {
            **parent,
            "index": self.index
        }

    def __eq__(self, other):
        if not(isinstance(other, GetRequest)):
            return False
        if not(self.request_type == other.request_type):
            return False
        if not(self.table == other.table):
            return False
        if not(len(self.index) == len(other.index)):
            return False
        for exp_key, exp_val in self.index.items():
            if not(other.index[exp_key] == exp_val):
                return False
        return True

class DeleteRequest(Request):
    def __init__(self, table: DatabaseTable, index: dict):
        super().__init__(RequestType.delete, table)
        self.index = index

    def dict(self):
        parent = super().dict()
        return {
            **parent,
            "index": self.index
        }

    def __eq__(self, other):
        if not(isinstance(other, DeleteRequest)):
            return False
        if not(self.request_type == other.request_type):
            return False
        if not(self.table == other.table):
            return False
        if not(len(self.index) == len(other.index)):
            return False
        for exp_key, exp_val in self.index.items():
            if not(other.index[exp_key] == exp_val):
                return False
        return True

