from enum import Enum
from uuid import uuid4
from discord import Member
from discord.ext import commands

from tools import is_empty_list
from api_ops.db import (
    GetRequest,
    AddRequest,
    DeleteRequest
)
from models.db import (
    DatabaseTable,
    Index
)
from one_night.cards import CardType
from one_night.core import (
    Player,
    GameConfig,
    Game
)
from managers import (
    DatabaseApiGateway,
    Settings
)

class ConfigOption(Enum):
    has_lone_wolf_rule = 0
    role_duration = 1
    day_phase_duration = 2
    vote_phase_duration = 3
    is_default_day_duration = 4

async def InsertRow(db_gateway: DatabaseApiGateway, row_data: dict, \
db_table: DatabaseTable):
    req = None
    if db_table == DatabaseTable.active_games \
    or db_table == DatabaseTable.game_configs \
    or db_table == DatabaseTable.admin_roles \
    or db_table == DatabaseTable.active_players:
        req = AddRequest(db_table, row_data)
    else:
        raise NotImplementedError(f"{db_table.name} is not a valid table.")
    req_id = uuid4().hex
    db_gateway.add_request(req, req_id)

async def DeleteRow(db_gateway: DatabaseApiGateway, index: dict, \
db_table: DatabaseTable):
    req = None
    if db_table == DatabaseTable.active_games \
    or db_table == DatabaseTable.game_configs \
    or db_table == DatabaseTable.admin_roles \
    or db_table == DatabaseTable.active_players:
        req = DeleteRequest(db_table, index)
    else:
        raise NotImplementedError(f"{db_table.name} is not a valid table.")
    req_id = uuid4().hex
    db_gateway.add_request(req, req_id)

async def GMsGameExistsInChannel(db_gateway: DatabaseApiGateway, server_id: int, \
game_master_id: int, channel_id: int):
    index = {
        Index.server_id.name: str(server_id),
        Index.game_master_id.name: str(game_master_id),
        Index.channel_id.name: str(channel_id)
    }
    req = GetRequest(DatabaseTable.active_games, index)
    req_id = uuid4().hex
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    row = resp["data"]
    return not(is_empty_list(row))

async def GetPlayers(db_gateway: DatabaseApiGateway, index: dict, bot: commands.Bot):
    req_id = uuid4().hex
    req = GetRequest(DatabaseTable.active_players, index)
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    rows = resp["data"]

    players = {}
    if is_empty_list(rows):
        return players

    if bot is None:
        for row in rows:
            id = int(row[1])
            players[id] = Player(id, None)
    else:
        for row in rows:
            id = int(row[1])
            user = await bot.fetch_user(id)
            players[id] = Player(id, user)
    return players

async def GetCardConfig(db_gateway: DatabaseApiGateway, index: dict, \
db_table: DatabaseTable):
    req_id = uuid4().hex
    req = GetRequest(db_table, index)
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    rows = resp["data"]

    config = {}
    if is_empty_list(rows):
        return config

    row = rows[0]
    if db_table == DatabaseTable.card_configs:
        for i in range(3,len(row)):
            if row[i] == 0:
                continue
            #CardType Enum Value == index in the CardConfig table
            config[CardType(i)] = row[i]
    elif db_table == DatabaseTable.active_card_configs:
        for i in range(1,len(row)):
            if row[i] == 0:
                continue
            #CardType Enum Value + 1 == index in the CardConfig table
            config[CardType(i+2)] = row[i]
    else:
        return NotImplementedError("Unimplemented table for getting card configs.")
    return config

async def GetGame(db_gateway: DatabaseApiGateway, game_master_id: int, \
bot: commands.Bot):
    index = { Index.game_master_id.name: str(game_master_id) }

    #get players
    players = []
    playerdict = await GetPlayers(db_gateway, index, bot)
    for _, player in playerdict.items():
        players.append(player)

    #get cards
    cards = await GetCardConfig(db_gateway, index, DatabaseTable.active_card_configs)
    
    #get config options
    req_id = uuid4().hex
    req = GetRequest(DatabaseTable.active_game_configs, index)
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    rows = resp["data"]
    if is_empty_list(rows):
        return None
    row = rows[0]

    config = GameConfig(cards, bool(row[3]), int(row[5]), int(row[4]), \
    int(row[6]))
    return Game(players, config, int(row[1]))

async def SearchGameEntryByPlayerId(db_gateway: DatabaseApiGateway, \
settings: Settings, user_id: int):
    index = { Index.user_id.name: str(user_id) }
    req_id = uuid4().hex
    req = GetRequest(DatabaseTable.active_players, index)
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    rows = resp["data"]

    if is_empty_list(rows):
        return None
    row = rows[0]

    game_master_id = int(row[0])
    if not(game_master_id in settings.GameManifest):
        return None

    return (game_master_id, settings.GameManifest[game_master_id])

async def ActiveGameExistsInChannel(db_gateway: DatabaseApiGateway, channel_id: int):
    index = { Index.channel_id.name: str(channel_id) }
    req = GetRequest(DatabaseTable.active_games, index)
    req_id = uuid4().hex
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    row = resp["data"]
    return not(is_empty_list(row))

async def IsGameAdmin(member: Member, db_gateway: DatabaseApiGateway):
    req_id = uuid4().hex
    index = {Index.none.name: 0}
    req = GetRequest(DatabaseTable.admin_roles, index)
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    rows = resp["data"]
    
    if is_empty_list(rows):
        return False
    for row in rows:
        role_id = int(row[1])
        for role in member.roles:
            if role.id == role_id:
                return True
    return False

async def RowExists(db_gateway: DatabaseApiGateway, index: dict, \
db_table: DatabaseTable):
    req_id = uuid4().hex
    req = GetRequest(db_table, index)
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    row = resp["data"]
    return not(is_empty_list(row))

async def IsTheirConfig(db_gateway: DatabaseApiGateway, server_id: int, \
author_id: int, config_name: str):
    index = {
        Index.server_id.name: str(server_id),
        Index.author_id.name: str(author_id),
        Index.game_config_name.name: config_name
    }
    return await RowExists(db_gateway, index, DatabaseTable.game_configs)
    
if __name__ == "__main__":
    pass
