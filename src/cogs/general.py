from discord.ext import (
    commands,
    tasks
)

from api_ops.dbops import DatabaseApiGateway
from managers import Settings

class HideableCog(commands.Cog):
    def __init__(self, bot: commands.Bot, settings: Settings, isHidden: bool = False):
        self.bot = bot
        self.settings = settings
        self.ishidden = isHidden

class BackgroundProcesses(HideableCog):
    def __init__(self, bot: commands.Bot, settings: Settings):
        super(BackgroundProcesses, self).__init__(bot, settings, True)
        self.TaskCleanup.start()
        self.Initialize.start()

    def cog_unload(self):
        self.TaskCleanup.cancel()
        self.Initialize.cancel()

    @tasks.loop(seconds=5)
    async def TaskCleanup(self):
        entriesToPop = []
        activeEntries = []
        for gmid, tasks in self.settings.task_manager.GameTasks.items():
            activeTasks = []
            for task in tasks:
                if task.cancelled() or task.done():
                    continue
                activeTasks.append(task)
            if activeTasks == []:
                entriesToPop.append(gmid)
            else:
                activeEntries.append({gmid: activeTasks})

        if not(activeEntries == []):
            for entry in activeEntries:
                self.settings.task_manager.GameTasks.update(entry)

        if not(entriesToPop == []):
            for entry in entriesToPop:
                self.settings.task_manager.GameTasks.pop(entry, None)

    @tasks.loop(count=1)
    async def Initialize(self):
        await self.settings.db_api_gateway.open(self.bot.loop)

if __name__ == "__main__":
    pass
