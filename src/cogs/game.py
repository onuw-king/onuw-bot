from asyncio import (
    gather,
    sleep,
    get_event_loop
)
from discord import (
    Member,
    User,
    Role,
    ChannelType,
    TextChannel
)
from discord.ext import commands
from discord.utils import get
from tabulate import tabulate
from datetime import datetime
from enum import Enum
from uuid import uuid4

import documentation.commands as cmddocs
import documentation.ConfigActions as configdocs
import documentation.GameActions as gamedocs
import documentation.GameSetupActions as gamesetupdocs
import documentation.GeneralActions as generaldocs
import documentation.Walkthroughs as walkdocs
from documentation.RoleSummaries import wakeorder
from tools import is_empty_list
from cogs.general import HideableCog
from api_ops.dbops import (
    ConfigOption,
    IsGameAdmin,
    RowExists,
    InsertRow,
    DeleteRow,
    GetCardConfig,
    GMsGameExistsInChannel,
    ActiveGameExistsInChannel,
    GetPlayers,
    GetGame
)
from managers import (
    DatabaseApiGateway,
    Settings
)
from api_ops.db import (
    GetRequest,
    UpdateRequest
)
from models.db import (
    Index,
    DatabaseTable
)
from one_night.core import (
    Player,
    IsValidCardPayload,
    PayloadToCardDict,
    HasTooManyOfCard,
    GetCardConstraints,
    CardDictToPayload,
    CalcPlayerCountFromCardDict
)

class HelpOptions(Enum):
    botsetup = 0
    configsetup = 1
    gamesetup = 2
    basicgameworkflow = 3
    availableroles = 4

### Helpers

def GameHasStarted(game_master_id: int, settings: Settings):
    if not(game_master_id in settings.game_manifest):
        return False
    return settings.game_manifest[game_master_id].HasStarted

def HasAdminPerms(member: Member):
    perms = member.guild_permissions
    for perm in perms:
        if perm[0] == "administrator" and perm[1]:
            return True
    return False

async def IsAdmin(member: Member, db_gateway: DatabaseApiGateway):
    hasAdminPerms = HasAdminPerms(member)
    isGameAdmin = await IsGameAdmin(member, db_gateway)
    return hasAdminPerms or isGameAdmin

### General Functions

async def AddAdminRole(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
server_id: int, role_id: int, role_name: str):
    has_ctx = not(ctx is None)
    row = { 
        Index.role_id.name: str(role_id),
        Index.server_id.name: str(server_id)
    }
    if not(await RowExists(db_gateway, row, DatabaseTable.admin_roles)):
        await InsertRow(db_gateway, row, DatabaseTable.admin_roles)
        if has_ctx:
            await ctx.send(f"{role_name} is now an admin role.")
    else:
        if has_ctx:
            await ctx.send(f"{role_name} is already an admin role.")

async def RemoveAdminRole(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
role_id: int, role_name: str):
    has_ctx = not(ctx is None)
    index = { Index.role_id.name: str(role_id) }
    if await RowExists(db_gateway, index, DatabaseTable.admin_roles):
        await DeleteRow(db_gateway, index, DatabaseTable.admin_roles)
        if has_ctx:
            await ctx.send(f"{role_name} is no longer an admin role.")
    else:
        if has_ctx:
            await ctx.send(f"{role_name} is not an admin role.")

### Game Config Functions

async def InitConfig(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
server_id: int, author_id: int, config_name: str, author_name: str):
    has_ctx = not(ctx is None)
    row = {
        Index.server_id.name: str(server_id),
        Index.author_id.name: str(author_id),
        Index.game_config_name.name: config_name,
        "author_name": author_name
    }
    if not(await RowExists(db_gateway, row, DatabaseTable.game_configs)):
        await InsertRow(db_gateway, row, DatabaseTable.game_configs)
        if has_ctx:
            await ctx.send(f"Added blank config {config_name}.")
    else:
        if has_ctx:
            await ctx.send(f"{config_name} already exists.")

async def DeleteConfig(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
server_id: int, author_id: int, config_name: str):
    has_ctx = not(ctx is None)
    index = {
        Index.server_id.name: str(server_id),
        Index.author_id.name: str(author_id),
        Index.game_config_name.name: config_name
    }
    if await RowExists(db_gateway, index, DatabaseTable.game_configs):
        await DeleteRow(db_gateway, index, DatabaseTable.game_configs)
        if has_ctx:
            await ctx.send(f"Deleted config {config_name}.")
    else:
        if has_ctx:
            await ctx.send(f"{config_name} is not a config.")

async def AddCardsToConfig(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
payload: str, server_id: int, author_id: int, config_name: str):
    #payload = "CardName:NumberOfCards;CardName:NumberOfCards"
    if not(IsValidCardPayload(payload)):
        if not(ctx is None):
            await ctx.send("Invalid card info payload: %s" % payload)
        return
    inCardDict = PayloadToCardDict(payload)

    #grab the previously committed card info from the database
    index = {
        Index.server_id.name: str(server_id),
        Index.author_id.name: str(author_id),
        Index.game_config_name.name: config_name
    }
    db_table = DatabaseTable.card_configs
    commCardDict = await GetCardConfig(db_gateway, index, db_table)
    orig_cards = \
    { card_type.name: num_cards for card_type, num_cards in commCardDict.items() }

    #iterate over the incoming dictionary and add the new cards to the
    #dictionary that came from the database
    for inCType, inNumCards in inCardDict.items():
        if inCType in commCardDict:
            totalCards = commCardDict[inCType] + inNumCards
            if not(HasTooManyOfCard(inCType, totalCards)):
                commCardDict[inCType] = totalCards
                if not(ctx is None):
                    uiMsg = "Staging addition of %d %s cards to %s... Final count will be %d..." % \
                    (inNumCards, inCType.name, config_name, totalCards)
                    await ctx.send(uiMsg)
            else:
                cConst = GetCardConstraints()
                cardMax = cConst[inCType].Value
                if not(ctx is None):
                    uiMsg = "Addition of %d %s cards not staged due to too many after addition. Max = %d. Total after addition would be %d." % \
                    (inNumCards, inCType.name, cardMax, totalCards)
                    await ctx.send(uiMsg)
        else:
            if not(HasTooManyOfCard(inCType, inNumCards)):
                commCardDict[inCType] = inNumCards
                if not(ctx is None):
                    uiMsg = "Staging addition of %d %s cards to %s..." % \
                    (inNumCards, inCType.name, config_name)
                    await ctx.send(uiMsg)
            else:
                cConst = GetCardConstraints()
                cardMax = cConst[inCType].Value
                if not(ctx is None):
                    uiMsg = "Addition of %d %s cards not staged due to too many after addition. Max = %d." % \
                    (inNumCards, inCType.name, cardMax)
                    await ctx.send(uiMsg)

    new_cards = \
    { card_type.name: num_cards for card_type, num_cards in commCardDict.items() }
    row = {
        **index,
        **new_cards
    }
    if not(new_cards == orig_cards):
        req = UpdateRequest(db_table, row, index)
        req_id = uuid4().hex
        db_gateway.add_request(req, req_id)
        if not(ctx is None):
            await ctx.send("Committed %s's staged cards changes." % config_name)
    else:
        if not(ctx is None):
            await ctx.send("Nothing to commit to %s." % config_name)

async def SubtractCardsFromConfig(ctx: commands.Context, \
db_gateway: DatabaseApiGateway, payload: str, server_id: int, author_id: int, \
config_name: str):
    #payload = "CardName:NumberOfCards;CardName:NumberOfCards"
    if not(IsValidCardPayload(payload)):
        if not(ctx is None):
            await ctx.send("Invalid card info payload: %s" % payload)
        return
    inCardDict = PayloadToCardDict(payload)

    #grab the previously committed card info from the database
    index = {
        Index.server_id.name: str(server_id),
        Index.author_id.name: str(author_id),
        Index.game_config_name.name: config_name
    }
    db_table = DatabaseTable.card_configs
    commCardDict = await GetCardConfig(db_gateway, index, db_table)
    orig_cards = \
    { card_type.name: num_cards for card_type, num_cards in commCardDict.items() }

    #iterate over the incoming dictionary and subtract cards from the
    #dictionary that came from the database
    for inCType, inNumCards in inCardDict.items():
        if inCType in commCardDict:
            totalCards = commCardDict[inCType] - inNumCards
            if totalCards >= 0:
                commCardDict[inCType] = totalCards
                if not(ctx is None):
                    uiMsg = "Staging removal of %d %s cards... %d will remain..." % \
                    (inNumCards, inCType.name, totalCards)
                    await ctx.send(uiMsg)
            else:
                numExistCards = commCardDict[inCType]
                if not(ctx is None):
                    uiMsg = "Removal of %d %s cards not staged due to total cards remaining being %d, which is less than zero." % \
                    (inNumCards, inCType.name, totalCards)
                    await ctx.send(uiMsg)
        else:
            if not(ctx is None):
                uiMsg = "Cannot stage removal of %s card because it does not exist in config %s." % \
                (inCType.name, config_name)
                await ctx.send(uiMsg)

    new_cards = \
    { card_type.name: num_cards for card_type, num_cards in commCardDict.items() }
    row = {
        **index,
        **new_cards
    }
    if not(new_cards == orig_cards):
        req = UpdateRequest(db_table, row, index)
        req_id = uuid4().hex
        db_gateway.add_request(req, req_id)
        if not(ctx is None):
            await ctx.send("Committed %s's staged cards changes." % config_name)
    else:
        if not(ctx is None):
            await ctx.send("Nothing to commit to %s." % config_name)

async def GetGameConfigOption(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
option: ConfigOption, server_id: int, author_id: int, config_name: str, \
author_name: str):
    index = { 
        Index.server_id.name: str(server_id),
        Index.author_id.name: str(author_id),
        Index.game_config_name.name: config_name
    }
    req_id = uuid4().hex
    req = GetRequest(DatabaseTable.game_configs, index)
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    rows = resp["data"]
    if is_empty_list(rows):
        if not(ctx is None):
            await ctx.send(f"Unable to find {author_name}'s config {config_name}.")
        return None

    #TODO: give specific column select functionality to api later
    col_num = None
    if option == ConfigOption.is_default_day_duration:
        col_num = 4
    elif option == ConfigOption.has_lone_wolf_rule:
        col_num = 5
    elif option == ConfigOption.role_duration:
        col_num = 6
    elif option == ConfigOption.day_phase_duration:
        col_num = 7
    elif option == ConfigOption.vote_phase_duration:
        col_num = 8
    else:
        raise NotImplementedError(f"{option.name} is not a recognized config option.")
    return rows[0][col_num]

async def SetGameConfigOption(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
option: ConfigOption, val, server_id: int, author_id: int, config_name: str, \
author_name: str):
    index = {
        Index.server_id.name: str(server_id),
        Index.author_id.name: str(author_id),
        Index.game_config_name.name: config_name
    }
    req = GetRequest(DatabaseTable.game_configs, index)
    req_id = uuid4().hex
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    rows = resp["data"]
    if is_empty_list(rows):
        if not(ctx is None):
            await ctx.send(f"Unable to find {author_name}'s config {config_name}")
        return
    row = rows[0]

    #TODO: return dictionary of values in the future for queries
    updated_row = {
        **index,
        "author_name": row[3],
        Index.player_count.name: row[4],
        ConfigOption.is_default_day_duration.name: row[5],
        ConfigOption.has_lone_wolf_rule.name: row[6],
        ConfigOption.role_duration.name: row[7],
        ConfigOption.day_phase_duration.name: row[8],
        ConfigOption.vote_phase_duration.name: row[9]
    }
    if option == ConfigOption.day_phase_duration:
        updated_row[ConfigOption.is_default_day_duration.name] = 0
    updated_row[option.name] = val
    req = UpdateRequest(DatabaseTable.game_configs, updated_row, index)
    req_id = uuid4().hex
    db_gateway.add_request(req, req_id)

# async def ClearCardConfig(db_gateway: DatabaseApiGateway, server_id: int, author_id: int, \
# gameConfigName: str):
#     sql = '''
#         UPDATE CardConfigs
#         SET
#         Villager=0,
#         Werewolf=0,
#         Seer=0,
#         Robber=0,
#         Troublemaker=0,
#         Tanner=0,
#         Drunk=0,
#         Hunter=0,
#         Mason=0,
#         Insomniac=0,
#         Minion=0,
#         Doppelganger=0
#         WHERE ServerID=%s
#         AND AuthorID=%s
#         AND GameConfigName=%s;
#     '''
#     data = (server_id, author_id, gameConfigName)
#     con = await db_gateway.GetConnection()
#     async with con.cursor() as curs:
#         await curs.execute(sql, data)
#     await con.commit()
#     db_gateway.ReleaseConnection(con)

async def RenameGameConfig(ctx: commands.Context, db_gateway: DatabaseApiGateway,
server_id: int, author_id: int, old_config_name: str, new_config_name: str):
    has_ctx = not(ctx is None)
    index = {
        Index.server_id.name: str(server_id),
        Index.author_id.name: str(author_id),
        Index.game_config_name.name: old_config_name
    }
    db_table = DatabaseTable.game_configs
    req = GetRequest(db_table, index)
    req_id = uuid4().hex
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    rows = resp["data"]
    if is_empty_list(rows):
        if has_ctx:
            await ctx.msg(f"{old_config_name} is not an existing config.")
        return

    row = rows[0]
    updated_index = {
        Index.server_id.name: str(server_id),
        Index.author_id.name: str(author_id),
        Index.game_config_name.name: new_config_name
    }
    #TODO: return dictionary of values in the future for queries
    updated_row = {
        **updated_index,
        "author_name": row[3],
        Index.player_count.name: row[4],
        ConfigOption.is_default_day_duration.name: row[5],
        ConfigOption.has_lone_wolf_rule.name: row[6],
        ConfigOption.role_duration.name: row[7],
        ConfigOption.day_phase_duration.name: row[8],
        ConfigOption.vote_phase_duration.name: row[9]
    }
    if not(await RowExists(db_gateway, updated_index, db_table)):
        req_id = uuid4().hex
        req = UpdateRequest(db_table, updated_row, index)
        db_gateway.add_request(req, req_id)
        if has_ctx:
            msg = f"Game config {old_config_name} was renamed to {new_config_name}."
            await ctx.send(msg)
    else:
        if has_ctx:
            msg = f"{new_config_name} is already an existing config of yours."
            await ctx.send(msg)

async def CopyGameConfig(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
server_id: int, from_author_id: int, from_config_name: str, target_author_id: int, \
target_config_name: str, from_author_name: str, target_author_name: str):
    has_ctx = not(ctx is None)
    #copy config data
    index = {
        Index.server_id.name: str(server_id),
        Index.author_id.name: str(from_author_id),
        Index.game_config_name.name: from_config_name
    }
    req_id = uuid4().hex
    game_config_table = DatabaseTable.game_configs
    req = GetRequest(game_config_table, index)
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    rows = resp["data"]
    if is_empty_list(rows):
        if has_ctx:
            await ctx.send(f"{from_config_name} from user {from_author_name} is not a saved game config.")
        return
    row = rows[0]

    #copy card data
    card_table = DatabaseTable.card_configs
    from_cards = await GetCardConfig(db_gateway, index, card_table)
    from_card_payload = CardDictToPayload(from_cards)

    #make blank config
    new_index = {
        Index.server_id.name: str(server_id),
        Index.author_id.name: str(target_author_id),
        Index.game_config_name.name: target_config_name
    }
    new_row = {
        **new_index,
        "author_name": target_author_name
    }
    if not(await RowExists(db_gateway, new_index, game_config_table)):
        await InsertRow(db_gateway, new_row, game_config_table)
    else:
        if has_ctx:
            await ctx.send(f"{target_config_name} is already one of your configs.")
        return

    #add the cards
    await AddCardsToConfig(ctx, db_gateway, from_card_payload, server_id, \
    target_author_id, target_config_name)
    
    #add the options
    #--don't update the player count because:
    #----1. users can't update it anyways
    #----2. updating it while updating day duration causes 
    #       issues with trigger that updates day duration based on player count.
    #----3. database updates it after cards are added anyways
    updated_row = {
        **new_row,
        Index.player_count.name: row[4],
        ConfigOption.is_default_day_duration.name: row[5],
        ConfigOption.has_lone_wolf_rule.name: row[6],
        ConfigOption.role_duration.name: row[7],
        ConfigOption.day_phase_duration.name: row[8],
        ConfigOption.vote_phase_duration.name: row[9]
    }
    req_id = uuid4().hex
    req = UpdateRequest(game_config_table, updated_row, new_index)
    db_gateway.add_request(req, req_id)
    
    if has_ctx:
        msg = f"Copied {from_author_name}'s config {from_config_name} to new config {target_config_name}." 
        await ctx.send(msg)

### Game Setup Functions

async def InitGame(ctx: commands.Context, settings: Settings, \
db_gateway: DatabaseApiGateway, server_id: int, game_master_id: int, chan_id: int, \
game_master_name: str):
    has_ctx = not(ctx is None)
    index = { Index.game_master_id.name: str(game_master_id) }
    db_table = DatabaseTable.active_games
    if not(await RowExists(db_gateway, index, db_table)):
        row = {
            Index.server_id.name: str(server_id),
            Index.game_master_id.name: str(game_master_id),
            Index.channel_id.name: str(chan_id),
            "game_master_name": game_master_name
        }
        await InsertRow(db_gateway, row, db_table)
        if has_ctx:
            await ctx.send(f"{game_master_name} initialized a new game.")
    else:
        if has_ctx:
            msg = f"{game_master_name} is already a game master in another active game."
            await ctx.send(msg)

async def DeleteGame(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
server_id: int, game_master_id: int, chan_id: int, game_master_name: str):
    has_ctx = not(ctx is None)
    index = {
        Index.server_id.name: str(server_id),
        Index.game_master_id.name: str(game_master_id),
        Index.channel_id.name: str(chan_id)
    }
    db_table = DatabaseTable.active_games
    if await RowExists(db_gateway, index, db_table):
        await DeleteRow(db_gateway, index, db_table)
        if has_ctx:
            await ctx.send(f"{game_master_name}'s game has been disbanded.")
    else:
        if has_ctx:
            await ctx.send(f"{game_master_name} does not have a game in this channel.")

async def AddPlayer(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
server_id: int, game_master_id: int, user_id: int, user_name: str):
    has_ctx = not(ctx is None)
    if not(game_master_id == user_id):
        index = { Index.user_id.name: str(user_id) }
        db_table = DatabaseTable.active_players
        if not(await RowExists(db_gateway, index, db_table)):
            row = {
                Index.game_master_id.name: str(game_master_id),
                Index.user_id.name: str(user_id),
                "user_name": user_name
            }
            await InsertRow(db_gateway, row, db_table)
            if has_ctx:
                await ctx.send(f"Added {user_name} to the game.")
        else:
            if has_ctx:
                await ctx.send(f"{user_name} is already in a game.")
    else:
        if has_ctx:
            await ctx.send(f"You cannot add yourself. Game masters are automatically added upon game initialization.")

async def RemovePlayer(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
serverid: int, game_master_id: int, user_id: int, user_name: str):
    has_ctx = not(ctx is None)
    if not(game_master_id == user_id):
        index = {
            Index.game_master_id.name: str(game_master_id),
            Index.user_id.name: str(user_id)
        }
        db_table = DatabaseTable.active_players
        if await RowExists(db_gateway, index, db_table):
            await DeleteRow(db_gateway, index, db_table)
            if has_ctx:
                await ctx.send(f"{user_name} has been removed from the game.")
        else:
            if has_ctx:
                await ctx.send(f"{user_name} is not in the game.")

    else:
        if has_ctx:
            await ctx.send(f"Game masters cannot remove themselves.")

async def AddCardsToGame(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
payload: str, server_id: int, game_master_id: int):
    #payload = "CardName:NumberOfCards;CardName:NumberOfCards"
    if not(IsValidCardPayload(payload)):
        if not(ctx is None):
            await ctx.send("Invalid card info payload: %s" % payload)
        return
    inCardDict = PayloadToCardDict(payload)

    #grab the previously committed card info from the database
    index = {
        Index.server_id.name: str(server_id),
        Index.game_master_id.name: str(game_master_id)
    }
    db_table = DatabaseTable.active_card_configs
    commCardDict = await GetCardConfig(db_gateway, index, db_table)
    orig_cards = \
    { card_type.name: num_cards for card_type, num_cards in commCardDict.items() }

    #iterate over the incoming dictionary and add the new cards to the
    #dictionary that came from the database
    for inCType, inNumCards in inCardDict.items():
        if inCType in commCardDict:
            totalCards = commCardDict[inCType] + inNumCards
            if not(HasTooManyOfCard(inCType, totalCards)):
                commCardDict[inCType] = totalCards
                if not(ctx is None):
                    uiMsg = "Staging addition of %d %s cards... Final count will be %d..." % \
                    (inNumCards, inCType.name, totalCards)
                    await ctx.send(uiMsg)
            else:
                cConst = GetCardConstraints()
                cardMax = cConst[inCType].Value
                if not(ctx is None):
                    uiMsg = "Addition of %d %s cards not staged due to too many after addition. Max = %d. Total after addition would be %d." % \
                    (inNumCards, inCType.name, cardMax, totalCards)
                    await ctx.send(uiMsg)
        else:
            if not(HasTooManyOfCard(inCType, inNumCards)):
                commCardDict[inCType] = inNumCards
                if not(ctx is None):
                    uiMsg = "Staging addition of %d %s cards...." % \
                    (inNumCards, inCType.name,)
                    await ctx.send(uiMsg)
            else:
                cConst = GetCardConstraints()
                cardMax = cConst[inCType].Value
                if not(ctx is None):
                    uiMsg = "Addition of %d %s cards not staged due to too many after addition. Max = %d." % \
                    (inNumCards, inCType.name, cardMax)
                    await ctx.send(uiMsg)

    new_cards = \
    { card_type.name: num_cards for card_type, num_cards in commCardDict.items() }
    row = {
        **index,
        **new_cards
    }
    if not(new_cards == orig_cards):
        req = UpdateRequest(db_table, row, index)
        req_id = uuid4().hex
        db_gateway.add_request(req, req_id)
        if not(ctx is None):
            await ctx.send("Committed staged cards changes.")
    else:
        if not(ctx is None):
            await ctx.send("Nothing to commit.")

async def SubtractCardsFromGame(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
payload: str, server_id: int, game_master_id: int):
    #payload = "CardName:NumberOfCards;CardName:NumberOfCards"
    if not(IsValidCardPayload(payload)):
        if not(ctx is None):
            await ctx.send("Invalid card info payload: %s" % payload)
        return
    inCardDict = PayloadToCardDict(payload)

    #grab the previously committed card info from the database
    index = {
        Index.server_id.name: str(server_id),
        Index.game_master_id.name: str(game_master_id)
    }
    db_table = DatabaseTable.active_card_configs
    commCardDict = await GetCardConfig(db_gateway, index, db_table)
    orig_cards = \
    { card_type.name: num_cards for card_type, num_cards in commCardDict.items() }

    #iterate over the incoming dictionary and subtract cards from the
    #dictionary that came from the database
    for inCType, inNumCards in inCardDict.items():
        if inCType in commCardDict:
            totalCards = commCardDict[inCType] - inNumCards
            if totalCards >= 0:
                commCardDict[inCType] = totalCards
                if not(ctx is None):
                    uiMsg = "Staging removal of %d %s cards... %d will remain..." % \
                    (inNumCards, inCType.name, totalCards)
                    await ctx.send(uiMsg)
            else:
                numExistCards = commCardDict[inCType]
                if not(ctx is None):
                    uiMsg = "Removal of %d %s cards not staged due to total cards remaining being %d, which is less than zero." % \
                    (inNumCards, inCType.name, totalCards)
                    await ctx.send(uiMsg)
        else:
            if not(ctx is None):
                uiMsg = "Cannot stage removal of %s card because there aren't any." % \
                (inCType.name,)
                await ctx.send(uiMsg)

    new_cards = \
    { card_type.name: num_cards for card_type, num_cards in commCardDict.items() }
    row = {
        **index,
        **new_cards
    }
    if not(new_cards == orig_cards):
        req = UpdateRequest(db_table, row, index)
        req_id = uuid4().hex
        db_gateway.add_request(req, req_id)
        if not(ctx is None):
            await ctx.send("Committed staged cards changes.")
    else:
        if not(ctx is None):
            await ctx.send("Nothing to commit.")

async def SaveActiveGameConfig(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
server_id: int, author_id: int, config_name: str, author_name: str):
    index = {
        Index.server_id.name: str(server_id),
        Index.game_master_id.name: str(author_id)
    }
    cardDict = await GetCardConfig(db_gateway, index, \
    DatabaseTable.active_card_configs)
    payload = CardDictToPayload(cardDict)

    req_id = uuid4().hex
    req = GetRequest(DatabaseTable.active_game_configs, index)
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    rows = resp["data"]
    if is_empty_list(rows):
        if not(ctx is None):
            await ctx.send("A game isn't in session.")
    row = rows[0]

    await InitConfig(ctx, db_gateway, server_id, author_id, config_name, author_name)
    await AddCardsToConfig(ctx, db_gateway, payload, server_id, author_id, config_name)

    index = {
        Index.server_id.name: str(server_id),
        Index.author_id.name: str(author_id),
        Index.game_config_name.name: config_name
    }
    updated_row = {
        **index,
        "author_name": author_name,
        Index.player_count.name: row[1],
        ConfigOption.is_default_day_duration.name: row[2],
        ConfigOption.has_lone_wolf_rule.name: row[3],
        ConfigOption.role_duration.name: row[4],
        ConfigOption.day_phase_duration.name: row[5],
        ConfigOption.vote_phase_duration.name: row[6]
    }
    req_id = uuid4().hex
    req = UpdateRequest(DatabaseTable.game_configs, updated_row, index)
    db_gateway.add_request(req, req_id)
    if not(ctx is None):
        await ctx.send(f"Current config saved as {config_name}.")

async def UseGameConfig(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
server_id: int, game_master_id: int, config_name: str):
    index = {
        Index.server_id.name: str(server_id),
        Index.author_id.name: str(game_master_id),
        Index.game_config_name.name: config_name
    }
    cardDict = await GetCardConfig(db_gateway, index, DatabaseTable.card_configs)
    payload = CardDictToPayload(cardDict)

    req_id = uuid4().hex
    req = GetRequest(DatabaseTable.game_configs, index)
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    rows = resp["data"]
    if is_empty_list(rows):
        if not(ctx is None):
            await ctx.send(f"{config_name} is not a saved config.")
        return
    row = rows[0]

    await ClearActiveCardConfig(db_gateway, game_master_id)
    await AddCardsToGame(ctx, db_gateway, payload, server_id, game_master_id)

    index = { Index.game_master_id.name: str(game_master_id) }
    updated_row = {
        **index,
        Index.player_count.name: row[4],
        ConfigOption.is_default_day_duration.name: row[5],
        ConfigOption.has_lone_wolf_rule.name: row[6],
        ConfigOption.role_duration.name: row[7],
        ConfigOption.day_phase_duration.name: row[8],
        ConfigOption.vote_phase_duration.name: row[9]
    }
    req_id = uuid4().hex
    req = UpdateRequest(DatabaseTable.active_game_configs, updated_row, index)
    db_gateway.add_request(req, req_id)
    if not(ctx is None):
        await ctx.send(f"Now using {config_name}.")

async def GetActiveGameConfigOption(ctx: commands.Context, \
db_gateway: DatabaseApiGateway, option: ConfigOption, server_id: int, \
game_master_id: int, game_master_name: str):
    index = { 
        Index.server_id.name: str(server_id),
        Index.game_master_id.name: str(game_master_id)
    }
    req_id = uuid4().hex
    req = GetRequest(DatabaseTable.active_game_configs, index)
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    rows = resp["data"]

    if is_empty_list(rows):
        if not(ctx is None):
            await ctx.send(f"Unable to find {game_master_name}'s active game config.")
        return

    #TODO: give specific column select functionality to api later
    col_num = None
    if option == ConfigOption.is_default_day_duration:
        col_num = 2
    elif option == ConfigOption.has_lone_wolf_rule:
        col_num = 3
    elif option == ConfigOption.role_duration:
        col_num = 4
    elif option == ConfigOption.day_phase_duration:
        col_num = 5
    elif option == ConfigOption.vote_phase_duration:
        col_num = 6
    else:
        raise NotImplementedError(f"{option.name} is not a recognized config option.")
    return rows[0][col_num]

async def SetActiveGameConfigOption(ctx: commands.Context, \
db_gateway: DatabaseApiGateway, option: ConfigOption, val, server_id: int, \
game_master_id: int, game_master_name: str):
    index = {
        Index.game_master_id.name: str(game_master_id)
    }
    req = GetRequest(DatabaseTable.active_game_configs, index)
    req_id = uuid4().hex
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    rows = resp["data"]
    if is_empty_list(rows):
        if not(ctx is None):
            await ctx.send(f"Unable to find {game_master_name}'s active game config.")
        return
    row = rows[0]

    #TODO: return dictionary of values in the future for queries
    updated_row = {
        **index,
        Index.player_count.name: row[1],
        ConfigOption.is_default_day_duration.name: row[2],
        ConfigOption.has_lone_wolf_rule.name: row[3],
        ConfigOption.role_duration.name: row[4],
        ConfigOption.day_phase_duration.name: row[5],
        ConfigOption.vote_phase_duration.name: row[6]
    }
    if option == ConfigOption.day_phase_duration:
        updated_row[ConfigOption.is_default_day_duration.name] = 0
    updated_row[option.name] = val
    req = UpdateRequest(DatabaseTable.active_game_configs, updated_row, index)
    req_id = uuid4().hex
    db_gateway.add_request(req, req_id)

async def ClearActiveCardConfig(db_gateway: DatabaseApiGateway, game_master_id: int):
    index = { Index.game_master_id.name: str(game_master_id) }
    req_id = uuid4().hex
    req = UpdateRequest(DatabaseTable.active_card_configs, index, index)
    db_gateway.add_request(req, req_id)

async def SetGameMaster(ctx: commands.Context, db_gateway: DatabaseApiGateway, \
game_master_id: int, new_game_master_id: int, new_game_master_name: str):
    index = { Index.game_master_id.name: str(game_master_id) }
    req_id = uuid4().hex
    req = GetRequest(DatabaseTable.active_games, index)
    db_gateway.add_request(req, req_id)
    resp = await db_gateway.get_response(req_id)
    rows = resp["data"]
    if is_empty_list(rows):
        if not(ctx is None):
            await ctx.send(f"You are not a game master.")
        return
    row = rows[0]
    
    updated_row = {
        Index.server_id.name: row[0],
        Index.game_master_id.name: str(new_game_master_id),
        Index.channel_id.name: row[2],
        "game_master_name": new_game_master_name
    }
    updated_index = {
        Index.server_id.name: row[0],
        **index,
        Index.channel_id.name: row[2] 
    }
    req_id = uuid4().hex
    req = UpdateRequest(DatabaseTable.active_games, updated_row, updated_index)
    db_gateway.add_request(req, req_id)
    if not(ctx is None):
        await ctx.send(f"Game Master has been passed to {new_game_master_name}.")

### Game Functions

async def BuildGame(ctx: commands.Context, bot: commands.Bot, \
db_gateway: DatabaseApiGateway, game_master_id: int):
    has_ctx = not(ctx is None)
    if has_ctx:
        await ctx.send("Building game...")
    game = await GetGame(db_gateway, game_master_id, bot)

    if game is None:
        if has_ctx:
            await ctx.send("Unable to find active game config while building game.")
        return

    if game.PlayerCount < game.MinPlayers:
        if has_ctx:
            await ctx.send(f"Not enough cards to start game. Minimum is {game.MinPlayers + 3}.")
        return None
    elif game.PlayerCount > game.MaxPlayers:
        if has_ctx:
            await ctx.send(f"Too many cards to start game. Maximum {game.MaxPlayers + 3}.")
        return None

    numPlayers = len(game.Players)
    if not(numPlayers == game.PlayerCount):
        if has_ctx:
            await ctx.send(f"This config is made for {game.PlayerCount} players, not {numPlayers}.")
        return None

    if has_ctx:
        await ctx.send("Shuffling deck...")
    game.ShuffleDeck(5)
    game.Deal()
    game.HasStarted = True
    game.InDealPhase = True
    return game

async def AbortGame(settings: Settings, game_master_id: int):
    settings.game_manifest.pop(game_master_id, None)
    settings.task_manager.AbortGameTasks(game_master_id)

async def DMInitialRole(player: Player):
    msg = f"You were dealt {type(player.Card).__name__}."
    msg += cmddocs.na_roleinfo[type(player.Card)]
    await player.User.send(msg)

async def SearchVoteResponse(ctx: commands.Context, player: Player, voteDuration: int):
    while(True):
        if player.TaskIterations == voteDuration and not(player.HasVoted):
            player.TaskIterations = 0
            await ctx.send(f"Extending voting duration for {player.User.name}. They haven't voted yet.")
            await player.User.send("Your time for voting has been extended because you haven't voted yet.")
        elif player.HasVoted or player.TaskIterations == voteDuration:
            player.TaskIterations = 0
            break
        else:
            player.TaskIterations += 1
        await sleep(1)

async def SearchNightActionResponse(player: Player, roleDuration: int):
    while(True):
        if player.HasDoneNightAction or \
        player.TaskIterations == roleDuration:
            player.TaskIterations = 0
            break
        else:
            player.TaskIterations += 1
        await sleep(1)

async def DoppelgangerNightSequence(cog: commands.Cog, ctx: commands.Context, \
player: Player, roleDuration: int, players: list, midCards: list):
    if player.HasDoneNightAction:
        return

    user = player.User
    msg = "Doppelganger, wake up.\n\nSelect a player to mimic their role.\n\n"
    iter = 0
    index = 0
    numPlayers = len(players)
    while (True):
        index += 1
        if iter == numPlayers:
            break
        if players[iter].Id == player.Id:
            index -= 1
        elif iter == numPlayers - 1:
            msg += "%d. %s" % (index,players[iter].User.name)
        else:
            msg += "%d. %s\n" % (index,players[iter].User.name)
        iter += 1
    msg += f"\n\n{cmddocs.na_ingame(cog.bot.command_prefix, cog.na.name)}"
    await user.send(msg)
    player.CanDoNightAction = True
    await SearchNightActionResponse(player, roleDuration)
    player.CanDoNightAction = False

    if player.Card.Appearance == Seer:
        msg = "Choose a player's card or a middle card to view.\n\n"
        numMCs = len(midCards)
        numPlayers = len(players)
        iter = 0
        index = 0
        while (True):
            index += 1
            if iter == numPlayers:
                break
            if players[iter].Id == player.Id:
                index -= 1
            else:
                msg += "%d. %s\n" % (index,players[iter].User.name)
            iter += 1

        for i in range(1, numMCs + 1):
            if i == numMCs:
                #need to exclude player from player list, so the count must be
                #numPlayers - 1
                msg += "%d. Middle Card %d" % (i + numPlayers - 1, i)
            else:
                msg += "%d. Middle Card %d\n" % (i + numPlayers - 1, i)
        msg += f"\n\n{cmddocs.na_ingame(cog.bot.command_prefix, cog.na.name)}"
        await user.send(msg)

        player.CanDoNightAction = True
        player.HasDoneNightAction = False
        await SearchNightActionResponse(player, roleDuration)
        player.CanDoNightAction = False
    elif player.Card.Appearance == Robber:
        msg = "Choose a person to rob or do nothing.\n\n"
        numPlayers = len(players)
        iter = 0
        index = 0
        while (True):
            index += 1
            if iter == numPlayers:
                break
            if players[iter].Id == player.Id:
                index -= 1
            else:
                msg = msg + "%d. %s\n" % (index,players[iter].User.name)
            iter += 1
        msg = msg + "%d. Do nothing" % (iter,)
        msg += f"\n\n{cmddocs.na_ingame(cog.bot.command_prefix, cog.na.name)}"
        await user.send(msg)

        player.CanDoNightAction = True
        player.HasDoneNightAction = False
        await SearchNightActionResponse(player, roleDuration)
        player.CanDoNightAction = False
    elif player.Card.Appearance == Troublemaker:
        msg = "Switch the roles of 2 players.\n\n"
        iter = 0
        index = 0
        numPlayers = len(players)
        while (True):
            index += 1
            if iter == numPlayers:
                break
            if players[iter].Id == player.Id:
                index -= 1
            elif iter == numPlayers - 1:
                msg += "%d. %s" % (index,players[iter].User.name)
            else:
                msg += "%d. %s\n" % (index,players[iter].User.name)
            iter += 1
        msg += f"\n\n{cmddocs.na_ingame(cog.bot.command_prefix, cog.na.name)}"
        await user.send(msg)

        player.CanDoNightAction = True
        player.HasDoneNightAction = False
        await SearchNightActionResponse(player, roleDuration)
        player.CanDoNightAction = False
    elif player.Card.Appearance == Drunk:
        msg = "Choose a middle card to replace your current card.\n\n"
        numMCs = len(midCards)
        for i in range(1, numMCs+1):
            if i == numMCs:
                msg += "%d.   Card %d" % (i,i)
            else:
                msg += "%d.   Card %d\n" % (i,i)
        msg += f"\n\n{cmddocs.na_ingame(cog.bot.command_prefix, cog.na.name)}"
        await user.send(msg)

        player.CanDoNightAction = True
        player.HasDoneNightAction = False
        await SearchNightActionResponse(player, roleDuration)
        if player.CanDoNightAction:
            dr = Drunk()
            dr.DefaultNightAction(player, midCards)
            await user.send("You were blackout drunk when you chose a card.")
        player.CanDoNightAction = False
    elif player.Card.Appearance == Werewolf or \
    player.Card.Appearance == Mason or \
    player.Card.Appearance == Insomniac:
        player.HasDoneNightAction = False

    msg = "Doppelganger, sleep."
    await user.send(msg)

async def WerewolfNightSequence(cog, ctx: commands.Context, player: Player, \
roleDuration: int, wwPlayers: list, usingLoneWolf: bool, midCards: list):
    if player.HasDoneNightAction:
        return

    user = player.User
    msg = "Werewolf, wake up."
    numWWs = len(wwPlayers)
    if numWWs > 1:
        msg += "\n\nThese are the other werewolves.\n\n"
        for i in range(0,numWWs):
            wwUser = wwPlayers[i].User
            if wwUser.id == player.User.id:
                continue

            if i == numWWs - 1:
                msg += wwUser.name
            else:
                msg += wwUser.name + "\n"
    else:
        msg += "\n\nYou are a lone wolf."
        if usingLoneWolf:
            msg += "\nChoose a middle card to view.\n\n"
            numMCs = len(midCards)
            for i in range(1, numMCs + 1):
                if i == numMCs:
                    msg += "%d.   Card %d" % (i,i)
                else:
                    msg += "%d.   Card %d\n" % (i,i)
            msg += f"\n\n{cmddocs.na_ingame(cog.bot.command_prefix, cog.na.name)}"
            await user.send(msg)
            player.CanDoNightAction = True
            await SearchNightActionResponse(player, roleDuration)
            player.CanDoNightAction = False
            msg = ""

    msg += "\n\nWerewolf, sleep."
    await user.send(msg)

async def MinionNightSequence(ctx: commands.Context, player: Player, roleDuration: int, \
wwPlayers: list):
    if player.HasDoneNightAction:
        return

    user = player.User
    msg = "Minion, wake up."
    if wwPlayers is None:
        msg += "\n\nThere aren't any werewolves."
    else:
        numWWs = len(wwPlayers)
        msg += "\n\nThese are the werewolves.\n\n"
        for i in range(0,numWWs):
            wwUser = wwPlayers[i].User
            if wwUser.id == player.User.id:
                continue

            if i == numWWs - 1:
                msg += wwUser.name
            else:
                msg += wwUser.name + "\n"

    msg += "\n\nMinion, sleep."
    await user.send(msg)

async def MasonNightSequence(ctx: commands.Context, player: Player, roleDuration: int, \
masPlayers: list):
    if player.HasDoneNightAction:
        return

    user = player.User
    msg = "Mason, wake up."
    numMas = len(masPlayers)
    if numMas == 1 and player in masPlayers:
        msg += "\n\nThere aren't any other masons."
    else:
        msg += "\n\nThese are the other masons.\n\n"
        for i in range(0,len(masPlayers)):
            masUser = masPlayers[i].User
            if masUser.id == player.User.id:
                continue
            if i == numMas - 1:
                msg += masUser.name
            else:
                msg += masUser.name + "\n"
    msg += "\n\nMason, sleep."
    await user.send(msg)

async def SeerNightSequence(cog, ctx: commands.Context, player: Player, \
roleDuration: int, players: list, midCards: list):
    if player.HasDoneNightAction:
        return

    user = player.User
    msg = "Seer, wake up.\nChoose a player's card or a middle card to view.\n\n"
    numMCs = len(midCards)
    numPlayers = len(players)
    iter = 0
    index = 0
    while (True):
        index += 1
        if iter == numPlayers:
            break
        if players[iter].Id == player.Id:
            index -= 1
        else:
            msg += "%d. %s\n" % (index,players[iter].User.name)
        iter += 1

    for i in range(1, numMCs + 1):
        if i == numMCs:
            #need to exclude player from player list, so the count must be
            #numPlayers - 1
            msg += "%d. Middle Card %d" % (i + numPlayers - 1, i)
        else:
            msg += "%d. Middle Card %d\n" % (i + numPlayers - 1, i)
    msg += f"\n\n{cmddocs.na_ingame(cog.bot.command_prefix, cog.na.name)}"
    await user.send(msg)
    player.CanDoNightAction = True
    await SearchNightActionResponse(player, roleDuration)
    player.CanDoNightAction = False

    msg = "Seer, sleep."
    await user.send(msg)

async def RobberNightSequence(cog, ctx: commands.Context, player: Player, \
roleDuration: int, players: list):
    if player.HasDoneNightAction:
        return

    user = player.User
    msg = "Robber, wake up.\n\nChoose a person to rob or do nothing.\n\n"
    numPlayers = len(players)
    iter = 0
    index = 0
    while (True):
        index += 1
        if iter == numPlayers:
            break
        if players[iter].Id == player.Id:
            index -= 1
        else:
            msg += "%d. %s\n" % (index,players[iter].User.name)
        iter += 1
    msg += "%d. Do nothing" % (iter,)
    msg += f"\n\n{cmddocs.na_ingame(cog.bot.command_prefix, cog.na.name)}"
    await user.send(msg)
    player.CanDoNightAction = True
    await SearchNightActionResponse(player, roleDuration)
    player.CanDoNightAction = False

    msg = "Robber, sleep."
    await user.send(msg)

async def TroublemakerNightSequence(cog, ctx: commands.Context, player: Player, \
roleDuration: int, players: list):
    if player.HasDoneNightAction:
        return

    user = player.User
    msg = "Troublemaker, wake up.\n\nSwitch the roles of 2 players.\n\n"
    iter = 0
    index = 0
    numPlayers = len(players)
    while (True):
        index += 1
        if iter == numPlayers:
            break
        if players[iter].Id == player.Id:
            index -= 1
        elif iter == numPlayers - 1:
            msg += "%d. %s" % (index,players[iter].User.name)
        else:
            msg += "%d. %s\n" % (index,players[iter].User.name)
        iter += 1
    msg += f"\n\n{cmddocs.na_ingame(cog.bot.command_prefix, cog.na.name)}"
    await user.send(msg)
    player.CanDoNightAction = True
    await SearchNightActionResponse(player, roleDuration)
    player.CanDoNightAction = False

    msg = "Troublemaker, sleep."
    await user.send(msg)

async def DrunkNightSequence(cog, ctx: commands.Context, player: Player, \
roleDuration: int, midCards: list):
    if player.HasDoneNightAction:
        return

    user = player.User
    msg = "Drunk, wake up.\n\nChoose a middle card to replace your current card.\n\n"
    numMCs = len(midCards)
    for i in range(1, numMCs+1):
        if i == numMCs:
            msg += "%d.   Card %d" % (i,i)
        else:
            msg += "%d.   Card %d\n" % (i,i)
    msg += f"\n\n{cmddocs.na_ingame(cog.bot.command_prefix, cog.na.name)}"
    await user.send(msg)
    player.CanDoNightAction = True
    await SearchNightActionResponse(player, roleDuration)
    if player.CanDoNightAction:
        player.Card.DefaultNightAction(player, midCards)
        await user.send("You were blackout drunk when you chose a card.")
    player.CanDoNightAction = False

    msg = "Drunk, sleep."
    await user.send(msg)

async def InsomniacNightSequence(ctx: commands.Context, player: Player, \
roleDuration: int):
    if player.HasDoneNightAction:
        return

    user = player.User
    msg = "Insomniac, wake up."
    msg += f"\n\nThis is your current role: {type(player.Card).__name__}"
    msg += "\n\nInsomniac, sleep."
    await user.send(msg)

### Commands

class GeneralActions(HideableCog, name="General Commands"):
    def __init__(self, bot, settings):
        super(GeneralActions, self).__init__(bot, settings)
        prefix = self.bot.command_prefix
        self.addadminroles.help = generaldocs.addadminroles(prefix, self.addadminroles.name)
        self.deleteadminroles.help = generaldocs.deleteadminroles(prefix, self.deleteadminroles.name)
        self.viewadminroles.help = generaldocs.viewadminroles(prefix, self.viewadminroles.name)

    @commands.command()
    async def help(self, ctx: commands.Context, command: str = None):
        if ctx.channel.type is ChannelType.private:
            return
            
        prefix = self.bot.command_prefix
        cogs = self.bot.cogs
        msg = ""
        if command is None:
            for cogName, cog in cogs.items():
                if cog.ishidden:
                    continue

                msg += f"{cogName}:\n"
                for cmd in cog.get_commands():
                    if cmd.name == "onenighthelp":
                        msg += f"    {prefix}{cmd}          Shows this message\n"
                    else:
                        msg += f"    {prefix}{cmd}\n"
            msg += f"\n\nType {prefix}onenighthelp commandname for more info on a specific command."
            msg += f"\n\nType {prefix}onenighthelp {HelpOptions.botsetup.name} for help on initial bot setup."
            msg += f"\n\nType {prefix}onenighthelp {HelpOptions.configsetup.name} for help on basic game config setup."
            msg += f"\n\nType {prefix}onenighthelp {HelpOptions.gamesetup.name} for help on basic game setup."
            msg += f"\n\nType {prefix}onenighthelp {HelpOptions.basicgameworkflow.name} for help on basic game workflow."
            msg += f"\n\nType {prefix}onenighthelp {HelpOptions.availableroles.name} to view all of the available roles."
        else:
            thiscmd = None
            isCommand = False
            for cmd in self.bot.commands:
                if cmd.name == command:
                    thiscmd = cmd
                    isCommand = True
                    break

            if command == HelpOptions.botsetup.name:
                msg += walkdocs.botsetup(prefix, self.addadminroles.name, \
                    self.addadminroles.name, self.addgamechannels.name)
            elif command == HelpOptions.configsetup.name:
                cog = cogs["Game Config Commands"]
                msg += walkdocs.configsetup(prefix, cog.initconfig.name, \
                    cog.configaddcards.name)
            elif command == HelpOptions.gamesetup.name:
                cog = cogs["Game Setup Commands"]
                msg += walkdocs.gamesetup(prefix, cog.initgame.name, \
                    cog.addcards.name, cog.addplayers.name)
            elif command == HelpOptions.basicgameworkflow.name:
                cog = cogs["Game Commands"]
                msg += walkdocs.basicgameworkflow(prefix, cog.deal.name, \
                    cog.start.name, cog.na.name, cog.vote.name)
            elif command == HelpOptions.availableroles.name:
                cardsToPrint = '\n'.join([ct.name for ct in CardType])
                msg += walkdocs.availableroles(cardsToPrint)
            elif not(isCommand):
                await ctx.send("%s is not a command." % command)
                return
            else:
                msg += f"{thiscmd.name}:\n"
                msg += f"{thiscmd.help}"

        await ctx.send("```" + msg + "```")

    @commands.command()
    async def addadminroles(self, ctx: commands.Context, *roles: Role):
        if ctx.channel.type is ChannelType.private:
            return
        print(ctx.channel.name)
        db_gateway = self.settings.db_api_gateway
        if not(await IsAdmin(ctx.author, db_gateway)):
            return
            
        for role in roles:
            await AddAdminRole(ctx, db_gateway, ctx.guild.id, \
            role.id, role.name)

    @commands.command()
    async def deleteadminroles(self, ctx: commands.Context, *roles: Role):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        if not(await IsAdmin(ctx.author, db_gateway)):
            return

        for role in roles:
            await RemoveAdminRole(ctx, db_gateway, role.id, role.name)

    @commands.command()
    async def viewadminroles(self, ctx):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        if not(await IsAdmin(ctx.author, db_gateway)):
            return

        server_id = ctx.guild.id
        index = {Index.server_id.name: server_id}
        req = GetRequest(DatabaseTable.admin_roles, index)
        req_id = uuid4().hex
        db_gateway.add_request(req, req_id)
        resp = await db_gateway.get_response(req_id)
        rows = resp["data"]

        heads = ["Role Name"]
        tablerows = []
        for row in rows:
            role = get(ctx.guild.roles, id=int(row[1]))
            tablerows.append([role.name])
        t = tabulate(tablerows, headers=heads, tablefmt="orgtbl")
        await ctx.send("```" + t + "```")

class ConfigActions(HideableCog, name="Game Config Commands"):
    def __init__(self, bot, settings):
        super(ConfigActions, self).__init__(bot, settings)
        prefix = self.bot.command_prefix
        self.initconfig.help = configdocs.initconfig(prefix, self.initconfig.name)
        self.deleteconfig.help = configdocs.deleteconfig(prefix, self.deleteconfig.name)
        self.forcedeleteconfig.help = configdocs.forcedeleteconfig(prefix, self.forcedeleteconfig.name)
        self.viewconfigs.help = configdocs.viewconfigs(prefix, self.viewconfigs.name)
        self.configaddcards.help = configdocs.configaddcards(prefix, self.configaddcards.name)
        self.configsubtractcards.help = configdocs.configsubtractcards(prefix, self.configsubtractcards.name)
        self.viewconfigcards.help = configdocs.viewconfigcards(prefix, self.viewconfigcards.name)
        self.configlonewolf.help = configdocs.configlonewolf(prefix, self.configlonewolf.name)
        self.configdayduration.help = configdocs.configdayduration(prefix, self.configdayduration.name)
        self.configroleduration.help = configdocs.configroleduration(prefix, self.configroleduration.name)
        self.configvoteduration.help = configdocs.configvoteduration(prefix, self.configvoteduration.name)
        self.renameconfig.help = configdocs.renameconfig(prefix, self.renameconfig.name)
        self.copyconfig.help = configdocs.copyconfig(prefix, self.copyconfig.name)

    @commands.command()
    async def initconfig(self, ctx: commands.Context, config_name: str):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        author_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, author_id, \
        chan_id)) and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return

        await InitConfig(ctx, db_gateway, server_id, author_id, config_name,
        ctx.message.author.name)

    @commands.command()
    async def deleteconfig(self, ctx: commands.Context, config_name: str):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        author_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, author_id, chan_id)) \
        and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return
        
        await DeleteConfig(ctx, db_gateway, server_id, author_id, config_name)

    @commands.command()
    async def forcedeleteconfig(self, ctx: commands.Context, config_name: str, \
    user: User):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        if not(await IsAdmin(ctx.author, db_gateway)):
            return

        await DeleteConfig(ctx, db_gateway, ctx.guild.id, user.id, config_name)

    @commands.command()
    async def viewconfigs(self, ctx: commands.Context, player_count: int = None):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        author_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, author_id, chan_id)) \
        and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return

        heads = ["Author", "Config Name", "Player Count"]
        tablerows = []
        index = None
        if player_count is None:
            index = { Index.server_id.name: str(server_id) }
        else:
            index = {
                Index.server_id.name: str(server_id),
                Index.player_count.name: player_count
            }
        req = GetRequest(DatabaseTable.game_configs, index)
        req_id = uuid4().hex
        db_gateway.add_request(req, req_id)
        resp = await db_gateway.get_response(req_id)
        result = resp["data"]
        for row in result:
            user_name = None
            user = self.bot.get_user(int(row[1]))
            if not(user is None):
                user_name = user.name
            else:
                user_name = row[3]
            tablerows.append([user_name, row[2], row[4]])
        t = tabulate(tablerows, headers=heads, tablefmt="orgtbl")
        await ctx.send("```" + t + "```")

    @commands.command()
    async def configaddcards(self, ctx: commands.Context, config_name: str, \
    payload: str):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        author_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, author_id, chan_id)) \
        and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return

        await AddCardsToConfig(ctx, db_gateway, payload, server_id, author_id, \
        config_name)

    @commands.command()
    async def configsubtractcards(self, ctx: commands.Context, config_name: str, \
    payload: str):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        author_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, author_id, chan_id)) \
        and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return

        await SubtractCardsFromConfig(ctx, db_gateway, payload, server_id, author_id, \
        config_name)

    @commands.command()
    async def viewconfigcards(self, ctx: commands.Context, config_name: str, \
    user: User = None):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        author_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, author_id, chan_id)) \
        and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return

        index = None
        if user is None:
            index = {
                Index.server_id.name: str(server_id),
                Index.author_id.name: str(author_id),
                Index.game_config_name.name: config_name
            }
        else:
            index = {
                Index.server_id.name: str(server_id),
                Index.author_id.name: str(user.id),
                Index.game_config_name.name: config_name
            }
        commCardDict = await GetCardConfig(db_gateway, index, \
        DatabaseTable.card_configs)

        rows = []
        commCardDict.pop(Index.server_id.name, None)
        commCardDict.pop(Index.author_id.name, None)
        commCardDict.pop(Index.game_config_name.name, None)
        for cardType, numCards in commCardDict.items():
            rows.append([cardType.name, str(numCards)])

        heads = ["Card Type", "Amount"]
        t = tabulate(rows, headers=heads, tablefmt="orgtbl")
        await ctx.send("```" + t + "```")

    @commands.command()
    async def configlonewolf(self, ctx: commands.Context, config_name, \
    boolVal: int = None):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        author_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, author_id, chan_id)) \
        and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return

        author_name = ctx.message.author.name
        option = ConfigOption.has_lone_wolf_rule
        if boolVal is None:
            val = await GetGameConfigOption(ctx, db_gateway, option, server_id, \
            author_id, config_name, author_name)
            await ctx.send(f"```{bool(val)}```")
        else:
            if not(boolVal == 0 or boolVal == 1):
                await ctx.send(f"{boolVal} is not a valid setting for the Lone Wolf option.")

            await SetGameConfigOption(ctx, db_gateway, option, boolVal, server_id, \
            author_id, config_name, author_name)

            if bool(boolVal):
                await ctx.send("Lone wolf rule is on.")
            else:
                await ctx.send("Lone wolf rule is off.")

    @commands.command()
    async def configdayduration(self, ctx: commands.Context, config_name, \
    dur: int = None):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        author_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, author_id, chan_id)) \
        and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return

        author_name = ctx.message.author.name
        option = ConfigOption.day_phase_duration
        if dur is None:
            val = await GetGameConfigOption(ctx, db_gateway, option, server_id, \
            author_id, config_name, author_name)
            await ctx.send(f"```{val} minutes```")
        else:
            if dur < 1:
                await ctx.send(f"{dur} is not a valid setting for the Day Duration option.")
                return

            await SetGameConfigOption(ctx, db_gateway, option, dur, server_id, \
            author_id, config_name, author_name)

            if dur == 1:
                await ctx.send(f"Game duration is {dur} minute.")
            else:
                await ctx.send(f"Game duration is {dur} minutes.")

    @commands.command()
    async def configroleduration(self, ctx: commands.Context, config_name, \
    dur: int = None):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        author_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, author_id, chan_id)) \
        and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return

        author_name = ctx.message.author.name
        option = ConfigOption.role_duration
        if dur is None:
            val = await GetGameConfigOption(ctx, db_gateway, option, server_id, \
            author_id, config_name, author_name)
            await ctx.send(f"```{int(val)} seconds```")
        else:
            if dur < 1:
                await ctx.send(f"{dur} is not a valid setting for the Role Duration option.")
                return

            await SetGameConfigOption(ctx, db_gateway, option, dur, server_id, \
            author_id, config_name, author_name)

            if dur == 1:
                await ctx.send(f"Role duration is {dur} second.")
            else:
                await ctx.send(f"Role duration is {dur} seconds.")

    @commands.command()
    async def configvoteduration(self, ctx: commands.Context, config_name, \
    dur: int = None):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        author_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, author_id, chan_id)) \
        and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return

        author_name = ctx.message.author.name
        option = ConfigOption.vote_phase_duration
        if dur is None:
            val = await GetGameConfigOption(ctx, db_gateway, option, server_id, \
            author_id, config_name, author_name)
            await ctx.send(f"```{int(val)} seconds```")
        else:
            if dur < 1:
                await ctx.send(f"{dur} is not a valid setting for the Vote Duration option.")
                return

            await SetGameConfigOption(ctx, db_gateway, option, dur, server_id, \
            author_id, config_name, author_name)

            if dur == 1:
                await ctx.send(f"Vote duration is {dur} second.")
            else:
                await ctx.send(f"Vote duration is {dur} seconds.")

    @commands.command()
    async def renameconfig(self, ctx: commands.Context, old_config_name: str, \
    new_config_name: str):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        author_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, author_id, chan_id)) \
        and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return

        await RenameGameConfig(ctx, db_gateway, server_id, author_id, old_config_name, \
        new_config_name)

    @commands.command()
    async def copyconfig(self, ctx: commands.Context, new_config_name: str, \
    from_user: User, from_config_name: str):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        author_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, author_id, chan_id)) \
        and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return
        
        await CopyGameConfig(ctx, db_gateway, server_id, from_user.id, \
        from_config_name, author_id, new_config_name, from_user.name, \
        ctx.message.author.name)

class GameSetupActions(HideableCog, name="Game Setup Commands"):
    def __init__(self, bot, settings):
        super(GameSetupActions, self).__init__(bot, settings)
        prefix = self.bot.command_prefix
        self.initgame.help = gamesetupdocs.initgame(prefix, self.initgame.name)
        self.disbandgame.help = gamesetupdocs.disbandgame(prefix, self.disbandgame.name)
        self.forcedisbandgame.help = gamesetupdocs.forcedisbandgame(prefix, self.forcedisbandgame.name)
        self.viewgames.help = gamesetupdocs.viewgames(prefix, self.viewgames.name)
        self.addplayers.help = gamesetupdocs.addplayers(prefix, self.addplayers.name)
        self.removeplayers.help = gamesetupdocs.removeplayers(prefix, self.removeplayers.name)
        self.viewplayers.help = gamesetupdocs.viewplayers(prefix, self.viewplayers.name)
        self.addcards.help = gamesetupdocs.addcards(prefix, self.addcards.name)
        self.subtractcards.help = gamesetupdocs.subtractcards(prefix, self.subtractcards.name)
        self.viewcards.help = gamesetupdocs.viewcards(prefix, self.viewcards.name)
        self.saveconfig.help = gamesetupdocs.saveconfig(prefix, self.saveconfig.name)
        self.useconfig.help = gamesetupdocs.useconfig(prefix, self.useconfig.name)
        self.lonewolf.help = gamesetupdocs.lonewolf(prefix, self.lonewolf.name)
        self.dayduration.help = gamesetupdocs.dayduration(prefix, self.dayduration.name)
        self.roleduration.help = gamesetupdocs.roleduration(prefix, self.roleduration.name)
        self.voteduration.help = gamesetupdocs.voteduration(prefix, self.voteduration.name)
        self.givegamemaster.help = gamesetupdocs.givegamemaster(prefix, self.givegamemaster.name)

    @commands.command()
    async def initgame(self, ctx: commands.Context):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)) and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return

        await InitGame(ctx, self.settings, db_gateway, server_id, game_master_id, \
        chan_id, ctx.message.author.name)

    @commands.command()
    async def disbandgame(self, ctx):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)) and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return

        await DeleteGame(ctx, db_gateway, server_id, game_master_id, \
        chan_id, ctx.message.author.name)

    @commands.command()
    async def forcedisbandgame(self, ctx: commands.Context, channel: TextChannel, \
    user: User):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = user.id
        chan_id = channel.id
        server_id = ctx.guild.id
        if not(await IsAdmin(ctx.author, db_gateway)):
            return

        await DeleteGame(ctx, db_gateway, server_id, game_master_id, chan_id, \
        user.name)

    @commands.command()
    async def viewgames(self, ctx: commands.Context):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)) and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return

        if GameHasStarted(game_master_id, self.settings):
            return

        heads = ["Game Master", "Channel"]
        tablerows = []
        index = { Index.server_id.name: str(server_id) }
        req_id = uuid4().hex
        req = GetRequest(DatabaseTable.active_games, index)
        db_gateway.add_request(req, req_id)
        resp = await db_gateway.get_response(req_id)
        rows = resp["data"]

        for row in rows:
            user_name = None
            user = self.bot.get_user(int(row[1]))
            if not(user is None):
                user_name = user.name
            else:
                user_name = row[3]
            chan = self.bot.get_channel(int(row[2]))
            tablerows.append([user_name, chan.name])
        t = tabulate(tablerows, headers=heads, tablefmt="orgtbl")
        await ctx.send("```" + t + "```")

    @commands.command()
    async def addplayers(self, ctx: commands.Context, *users: User):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)) and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return

        if GameHasStarted(game_master_id, self.settings):
            return

        for user in users:
            await AddPlayer(ctx, db_gateway, server_id, game_master_id, user.id, \
            user.name)

    @commands.command()
    async def removeplayers(self, ctx: commands.Context, *users: User):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)) and await ActiveGameExistsInChannel(db_gateway, chan_id):
            return

        if GameHasStarted(game_master_id, self.settings):
            return

        for user in users:
            await RemovePlayer(ctx, db_gateway, server_id, game_master_id, user.id, \
            user.name)

    @commands.command()
    async def viewplayers(self, ctx: commands.Context):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)):
            return

        if GameHasStarted(game_master_id, self.settings):
            return

        heads = ["Players"]
        tablerows = []
        index = { Index.game_master_id.name: game_master_id }
        req_id = uuid4().hex
        req = GetRequest(DatabaseTable.active_players, index)
        db_gateway.add_request(req, req_id)
        resp = await db_gateway.get_response(req_id)
        rows = resp["data"]
        for row in rows:
            user_name = None
            user = self.bot.get_user(int(row[1]))
            if not(user is None):
                user_name = user.name
            else:
                user_name = row[2]
            tablerows.append([user_name])

        t = tabulate(tablerows, headers=heads, tablefmt="orgtbl")
        await ctx.send("```" + t + "```")

    @commands.command()
    async def addcards(self, ctx: commands.Context, payload: str):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)):
            return

        if GameHasStarted(game_master_id, self.settings):
            return

        await AddCardsToGame(ctx, db_gateway, payload, server_id, game_master_id)

        #report how many cards needed to satisfy the number of players in the game
        index = {
            Index.server_id.name: str(server_id),
            Index.game_master_id.name: str(game_master_id)
        }
        numPlayers = len(await GetPlayers(db_gateway, index, None))
        cardDict = await GetCardConfig(db_gateway, index, \
        DatabaseTable.active_card_configs)
        curPlayerCount = CalcPlayerCountFromCardDict(cardDict)

        if numPlayers > curPlayerCount:
            await ctx.send(f"Need {numPlayers - curPlayerCount} more cards to satisfy the player count constraint.")
        elif numPlayers < curPlayerCount:
            await ctx.send(f"Remove {curPlayerCount - numPlayers} cards to satisfy the player count constraint.")
        else:
            await ctx.send("Player count constraint is met; no more cards are needed.")

    @commands.command()
    async def subtractcards(self, ctx: commands.Context, payload: str):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)):
            return

        if GameHasStarted(game_master_id, self.settings):
            return

        await SubtractCardsFromGame(ctx, db_gateway, payload, server_id, \
        game_master_id)

        #report how many cards needed to satisfy the number of players in the game
        index = {
            Index.server_id.name: str(server_id),
            Index.game_master_id.name: str(game_master_id)
        }
        numPlayers = len(await GetPlayers(db_gateway, index, None))
        cardDict = await GetCardConfig(db_gateway, index, \
        DatabaseTable.active_card_configs)
        curPlayerCount = CalcPlayerCountFromCardDict(cardDict)

        if numPlayers > curPlayerCount:
            await ctx.send(f"Need {numPlayers - curPlayerCount} more cards to satisfy the player count constraint.")
        elif numPlayers < curPlayerCount:
            await ctx.send(f"Remove {curPlayerCount - numPlayers} cards to satisfy the player count constraint.")
        else:
            await ctx.send("Player count constraint is met; no more cards are needed.")

    @commands.command()
    async def viewcards(self, ctx):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)):
            return

        if GameHasStarted(game_master_id, self.settings):
            return

        index = {
            Index.server_id.name: str(server_id),
            Index.game_master_id.name: str(game_master_id)
        }
        commCardDict = await GetCardConfig(db_gateway, index, \
        DatabaseTable.active_card_configs)

        rows = []
        for cardType, numCards in commCardDict.items():
            rows.append([cardType.name, str(numCards)])

        heads = ["Card Type", "Amount"]
        t = tabulate(rows, headers=heads, tablefmt="orgtbl")
        await ctx.send("```" + t + "```")

    @commands.command()
    async def saveconfig(self, ctx: commands.Context, config_name: str):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)):
            return

        if GameHasStarted(game_master_id, self.settings):
            return

        await SaveActiveGameConfig(ctx, db_gateway, server_id, game_master_id, \
        config_name, ctx.message.author.name)

    @commands.command()
    async def useconfig(self, ctx: commands.Context, config_name: str):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)):
            return

        index = {
            Index.server_id.name: str(server_id),
            Index.author_id.name: str(game_master_id),
            Index.game_config_name.name: config_name
        }
        if not(await RowExists(db_gateway, index, DatabaseTable.game_configs)):
            await ctx.send(f"{config_name} is not a saved config.")
            return

        if GameHasStarted(game_master_id, self.settings):
            return

        await UseGameConfig(ctx, db_gateway, server_id, game_master_id, config_name)

    @commands.command()
    async def lonewolf(self, ctx: commands.Context, boolVal: int = None):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)):
            return

        if GameHasStarted(game_master_id, self.settings):
            return

        option = ConfigOption.has_lone_wolf_rule
        if boolVal is None:
            val = await GetActiveGameConfigOption(ctx, db_gateway, option, server_id, \
            game_master_id, ctx.message.author.name)
            await ctx.send(f"```{bool(val)}```")
        else:
            if not(boolVal == 0 or boolVal == 1):
                await ctx.send(f"{boolVal} is not a valid setting for the Lone Wolf option.")

            await SetActiveGameConfigOption(ctx, db_gateway, option, boolVal, server_id, \
            game_master_id, ctx.message.author.name)

            if boolVal == 1:
                await ctx.send("Lone wolf rule is on.")
            elif boolVal == 0:
                await ctx.send("Lone wolfe rule is off.")

    @commands.command()
    async def dayduration(self, ctx: commands.Context, dur: int = None):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)):
            return

        if GameHasStarted(game_master_id, self.settings):
            return

        option = ConfigOption.day_phase_duration
        if dur is None:
            val = await GetActiveGameConfigOption(ctx, db_gateway, option, server_id, \
            game_master_id, ctx.message.author.name)

            await ctx.send(f"```{int(val)} minutes```")
        else:
            if dur < 1:
                await ctx.send(f"{dur} is not a valid setting for the Day Duration option.")
                return

            await SetActiveGameConfigOption(ctx, db_gateway, option, dur, server_id, \
            game_master_id, ctx.message.author.name)

            if dur == 1:
                await ctx.send(f"Game duration is {dur} minute.")
            else:
                await ctx.send(f"Game duration is {dur} minutes.")

    @commands.command()
    async def roleduration(self, ctx: commands.Context, dur: int = None):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)):
            return

        if GameHasStarted(game_master_id, self.settings):
            return

        option = ConfigOption.role_duration
        if dur is None:
            val = await GetActiveGameConfigOption(ctx, db_gateway, option, server_id, \
            game_master_id, ctx.message.author.name)
            await ctx.send(f"```{int(val)} seconds```")
        else:
            if dur < 1:
                await ctx.send(f"{dur} is not a valid setting for the Role Duration option.")
                return

            await SetActiveGameConfigOption(ctx, db_gateway, option, dur, server_id, \
            game_master_id, ctx.message.author.name)

            if dur == 1:
                await ctx.send(f"Role duration is {dur} second.")
            else:
                await ctx.send(f"Role duration is {dur} seconds.")

    @commands.command()
    async def voteduration(self, ctx: commands.Context, dur: int = None):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)):
            return

        if GameHasStarted(game_master_id, self.settings):
            return

        option = ConfigOption.vote_phase_duration
        if dur is None:
            val = await GetActiveGameConfigOption(ctx, db_gateway, option, server_id, \
            game_master_id, ctx.message.author.name)
            await ctx.send(f"```{int(val)} seconds```")
        else:
            if dur < 1:
                await ctx.send(f"{dur} is not a valid setting for the Vote Duration option.")
                return

            await SetActiveGameConfigOption(ctx, db_gateway, option, dur, server_id, \
            game_master_id, ctx.message.author.name)

            if dur == 1:
                await ctx.send(f"Vote duration is {dur} second.")
            else:
                await ctx.send(f"Vote duration is {dur} seconds.")

    @commands.command()
    async def givegamemaster(self, ctx: commands.Context, user: User):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)):
            return

        if GameHasStarted(game_master_id, self.settings):
            return

        await SetGameMaster(ctx, db_gateway, game_master_id, user.id, user.name)

class GameActions(HideableCog, name="Game Commands"):
    def __init__(self, bot, settings):
        super(GameActions, self).__init__(bot, settings)
        prefix = self.bot.command_prefix
        self.deal.help = gamedocs.deal(prefix, self.deal.name)
        # self.start.help = gamedocs.start(prefix, self.start.name)
        self.abort.help = gamedocs.abort(prefix, self.abort.name)
        # self.na.help = gamedocs.na(prefix, self.na.name)
        # self.vote.help = gamedocs.vote(prefix, self.vote.name)

    @commands.command()
    async def deal(self, ctx):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)):
            return

        if GameHasStarted(game_master_id, self.settings):
            return

        game = await BuildGame(ctx, self.bot, db_gateway, game_master_id)
        if game is None:
            return
        self.settings.game_manifest[game_master_id] = game

        loop = get_event_loop()
        tasks = [loop.create_task(DMInitialRole(p)) for p in game.Players]
        self.settings.task_manager.AddGameTasks(tasks, game_master_id)
        await gather(*tasks)

        await ctx.send(wakeorder(self.bot.command_prefix))

    @commands.command()
    async def start(self, ctx):
        if ctx.channel.type is ChannelType.private:
            return

        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)):
            return

        if not(GameHasStarted(game_master_id, self.settings)):
            return

        game = self.settings.game_manifest[game_master_id]
        if not(game.InDealPhase):
            return

        #NIGHT ACTION PHASE
        game.InNighttime = True
        game.InDealPhase = False
        await ctx.send("The night has begun.")

        dgPlayers = game.GetPlayersFromInitRole(Doppelganger)
        wwPlayers = game.GetPlayersFromInitRole(Werewolf)
        minPlayers = game.GetPlayersFromInitRole(Minion)
        masPlayers = game.GetPlayersFromInitRole(Mason)
        seerPlayers = game.GetPlayersFromInitRole(Seer)
        robPlayers = game.GetPlayersFromInitRole(Robber)
        tmPlayers = game.GetPlayersFromInitRole(Troublemaker)
        drkPlayers = game.GetPlayersFromInitRole(Drunk)
        insPlayers = game.GetPlayersFromInitRole(Insomniac)

        print("Players before:")
        for p in game.Players:
            print(f"{p.User.name}: {type(p.Card).__name__}")
        print("Middle cards before:")
        for c in game.MiddleCards:
            print(type(c).__name__)

        loop = get_event_loop()
        roleDur = game.Config.RoleDuration
        usingLoneWolf = game.Config.HasLoneWolf
        if not(dgPlayers is None):
            tasks = [loop.create_task(DoppelgangerNightSequence(self, ctx, player, \
            roleDur, game.Players, game.MiddleCards)) for player in dgPlayers]
            self.settings.task_manager.AddGameTasks(tasks, game_master_id)
            await gather(*tasks)

            for dgPlayer in dgPlayers:
                if dgPlayer.Card.Appearance == Werewolf:
                    wwPlayers.append(dgPlayer)
                elif dgPlayer.Card.Appearance == Mason:
                    masPlayers.append(dgPlayer)
                elif dgPlayer.Card.Appearance == Insomniac:
                    insPlayers.append(dgPlayer)

        if not(wwPlayers is None):
            tasks = [loop.create_task(WerewolfNightSequence(self, ctx, player, \
            roleDur, wwPlayers, usingLoneWolf, game.MiddleCards)) \
            for player in wwPlayers]
            self.settings.task_manager.AddGameTasks(tasks, game_master_id)
            await gather(*tasks)

        if not(minPlayers is None):
            tasks = [loop.create_task(MinionNightSequence(ctx, player, roleDur, \
            wwPlayers)) for player in minPlayers]
            self.settings.task_manager.AddGameTasks(tasks, game_master_id)
            await gather(*tasks)

        if not(masPlayers is None):
            tasks = [loop.create_task(MasonNightSequence(ctx, player, roleDur, \
            masPlayers)) for player in masPlayers]
            self.settings.task_manager.AddGameTasks(tasks, game_master_id)
            await gather(*tasks)

        if not(seerPlayers is None):
            tasks = [loop.create_task(SeerNightSequence(self, ctx, player, roleDur, \
            game.Players, game.MiddleCards)) for player in seerPlayers]
            self.settings.task_manager.AddGameTasks(tasks, game_master_id)
            await gather(*tasks)

        if not(robPlayers is None):
            tasks = [loop.create_task(RobberNightSequence(self, ctx, player, roleDur, \
            game.Players)) for player in robPlayers]
            self.settings.task_manager.AddGameTasks(tasks, game_master_id)
            await gather(*tasks)

        if not(tmPlayers is None):
            tasks = [loop.create_task(TroublemakerNightSequence(self, ctx, player, \
            roleDur, game.Players)) for player in tmPlayers]
            self.settings.task_manager.AddGameTasks(tasks, game_master_id)
            await gather(*tasks)

        if not(drkPlayers is None):
            tasks = [loop.create_task(DrunkNightSequence(self, ctx, player, roleDur, \
            game.MiddleCards)) for player in drkPlayers]
            self.settings.task_manager.AddGameTasks(tasks, game_master_id)
            await gather(*tasks)

        if not(insPlayers is None):
            tasks = [loop.create_task(InsomniacNightSequence(ctx, player, roleDur)) \
            for player in insPlayers]
            self.settings.task_manager.AddGameTasks(tasks, game_master_id)
            await gather(*tasks)

        print("\nPlayers after:")
        for p in game.Players:
            cName = type(p.Card).__name__
            print("%s: %s" % (p.User.name,cName))
        print("Middle cards after:")
        for c in game.MiddleCards:
            print(type(c).__name__)
        
        #DISCUSSION PHASE
        game.InNighttime = False
        game.InDaytime = True
        await ctx.send("Everyone wake up; night has ended.")
        await sleep(game.Config.DayPhaseDuration * 60)

        #VOTING PHASE
        voteDur = game.Config.VotingPhaseDuration
        game.InDaytime = False
        game.InVoting = True
        msg = "The time for talk is over. Vote for who you should kill.\n\n"
        numPlayers = len(game.Players)
        for i in range(0,numPlayers):
            if i == numPlayers - 1:
                msg += "%d. %s" % (i+1, game.Players[i].User.name)
            else:
                msg += "%d. %s\n" % (i+1, game.Players[i].User.name)
        msg += f"\n\nVote by DMing the {self.bot.command_prefix}{self.vote.name} command to the bot (me)."
        await ctx.send(msg)

        tasks = []
        for p in game.Players:
            p.CanVote = True
            tasks.append(loop.create_task(SearchVoteResponse(ctx, p, voteDur)))
        self.settings.task_manager.AddGameTasks(tasks, game_master_id)
        await gather(*tasks)

        #POST GAME PHASE
        game.InVoting = False
        await ctx.send("All votes are in.")

        tallyHeaders = ["Name", "Votes", "Role"]
        tallyRows = []
        tally = {}
        voterHeaders = ["Name"]
        voterTables = {}
        votes = game.GetVotes()
        for player in game.Players:
            count = 0
            for vote in votes:
                if vote.Candidate.Id == player.Id:
                    count += 1
            tally[player] = count
        for player, voteCount in tally.items():
            #building tally table
            playerName = player.User.name
            role = type(player.Card).__name__
            tallyRows.append([playerName, str(voteCount), role])

            #building voter tables
            if voteCount == 0:
                continue
            voterRows = []
            for vote in votes:
                if not(vote.Candidate.Id == player.Id):
                    continue
                voterRows.append([vote.Voter.User.name])
            voterTable = tabulate(voterRows, headers=voterHeaders, tablefmt="orgtbl")
            voterTables.update({player: voterTable})
        tallyTable = tabulate(tallyRows, headers=tallyHeaders, tablefmt="orgtbl")
        await ctx.send("Vote Tally\n```" + tallyTable + "```")

        #send the tables to show who voted for whom
        for player, table in voterTables.items():
            msg = "Voters for %s:\n" % player.User.name
            msg = msg + "```" + table + "```"
            await ctx.send(msg)

        #determine who's dead
        for player, voteCount in tally.items():
            if not(voteCount > 1):
                continue
            player.IsAlive = False
            if isinstance(player.Card, Hunter):
                player.Vote.Candidate.IsAlive = False
        
        #gather team members
        tanners = []
        werewolves = []
        villagers = []
        for player in game.Players:
            if player.Card.Team == Team.Villagers:
                villagers.append(player)
            elif player.Card.Team == Team.Werewolves:
                werewolves.append(player)
            elif player.Card.Team == Team.Tanner:
                tanners.append(player)
        
        #determine which team won
        deadTanners = 0
        numTanners = len(tanners)
        deadVillagers = 0
        numVillagers = len(villagers)
        deadWWs = 0
        numWWs = len(werewolves)
        for tanner in tanners:
            if not(tanner.IsAlive):
                deadTanners += 1
        for villager in villagers:
            if not(villager.IsAlive):
                deadVillagers += 1
        for ww in werewolves:
            if not(ww.IsAlive) and not(isinstance(ww.Card, Minion)):
                deadWWs += 1

        allTannersDead = deadTanners >= 1 and \
        deadTanners == numTanners

        allWWsAndTannersAlive = deadWWs == 0 and \
        not(allTannersDead)

        anyDeadWWs = deadWWs > 0
        noWWs = numWWs == 0
        allVillagersAlive = deadVillagers == 0

        if allTannersDead and anyDeadWWs:
            await ctx.send("Tanners and Villagers win!")
        elif allTannersDead:
            await ctx.send("Tanners win!")
        elif allWWsAndTannersAlive:
            await ctx.send("Werewolves win!")
        elif anyDeadWWs or  \
        (noWWs and allVillagersAlive):
            await ctx.send("Villagers win!")
        else:
            await ctx.send("Draw")
        
        game.HasStarted = False
        await AbortGame(self.settings, game_master_id)

    @commands.command()
    async def abort(self, ctx):
        if ctx.channel.type is ChannelType.private:
            return
            
        db_gateway = self.settings.db_api_gateway
        game_master_id = ctx.message.author.id
        chan_id = ctx.message.channel.id
        server_id = ctx.guild.id
        if not(await GMsGameExistsInChannel(db_gateway, server_id, game_master_id, \
        chan_id)):
            return

        if not(GameHasStarted(game_master_id, self.settings)):
            return

        await AbortGame(self.settings, game_master_id)
        await ctx.send("Game aborted.")

    @commands.command()
    async def na(self, ctx: commands.Context, *inputs: int):
        if not(ctx.channel.type is ChannelType.private):
            return

        db_gateway = self.settings.db_gateway
        dataTup = await SearchGameEntryByPlayerId(db_gateway, self.settings, \
        ctx.author.id)
        if dataTup is None:
            return
        game_master_id = dataTup[0]
        game = dataTup[1]

        if not(game.InNighttime):
            return

        if not(GameHasStarted(game_master_id, self.settings)):
            return

        player = None
        for p in game.Players:
            if p.User.id == ctx.author.id:
                player = p
                break

        if player.HasDoneNightAction or not(player.CanDoNightAction):
            return

        user = player.User
        if isinstance(player.InitialRole, Werewolf) or \
        player.Card.Appearance == Werewolf:
            if len(inputs) < 1:
                await user.send("Too few options specified. You can only choose one.")
                return
            elif len(inputs) > 1:
                await user.send("Too many options specified. You can only choose one.")
                return

            #this should be the only input
            cardIndex = inputs[0] - 1
            if not(cardIndex in range(0, len(game.MiddleCards))):
                await user.send("You must pick one of the displayed options.")
                return

            cName = type(game.MiddleCards[cardIndex]).__name__
            await user.send("You've chosen to see: %s." % cName)
            player.CanDoNightAction = False
            player.HasDoneNightAction = True
        elif isinstance(player.InitialRole, Seer) or \
        player.Card.Appearance == Seer:
            if len(inputs) < 1:
                await user.send("Too few options specified. You can only choose one.")
                return
            elif len(inputs) > 1:
                await user.send("Too many options specified. You can only choose one.")
                return

            #this should be the only input
            index = inputs[0] - 1
            numMCs = len(game.MiddleCards)
            numPlayers = len(game.Players)
            if not(index in range(0, numMCs + numPlayers - 1)):
                await user.send("You must pick one of the displayed options.")
                return

            if index in range(0, numPlayers - 1):
                otherPlayers = []
                for p in game.Players:
                    if p.Id == user.id:
                        continue
                    otherPlayers.append(p)

                otherPlayerName = otherPlayers[index].User.name
                cName = type(otherPlayers[index].Card).__name__
                await user.send("%s's role: %s" % (otherPlayerName,cName))
                player.CanDoNightAction = False
                player.HasDoneNightAction = True
            elif index in range(numPlayers - 1, numMCs + numPlayers - 1):
                index -= numPlayers - 1
                cName = type(game.MiddleCards[index]).__name__
                await user.send("You've chosen to see: %s." % cName)
                player.CanDoNightAction = False
                player.HasDoneNightAction = True
            else:
                await user.send("Weird input. Try again.")
        elif isinstance(player.InitialRole, Drunk) or \
        player.Card.Appearance == Drunk:
            if len(inputs) < 1:
                await user.send("Too few options specified. You can only choose one.")
                return
            elif len(inputs) > 1:
                await user.send("Too many options specified. You can only choose one.")
                return

            #this should be the only input
            cardIndex = inputs[0] - 1
            if not(cardIndex in range(0, len(game.MiddleCards))):
                await user.send("You must pick one of the displayed options.")
                return

            oldCard = player.Card
            newCard = game.MiddleCards[cardIndex]
            game.MiddleCards[cardIndex] = oldCard
            player.Card = newCard
            await user.send("You chose a new role, but you're too drunk to remember what it is.")
            player.CanDoNightAction = False
            player.HasDoneNightAction = True
        elif isinstance(player.InitialRole, Robber) or \
        player.Card.Appearance == Robber:
            if len(inputs) < 1:
                await user.send("Too few options specified. You can only choose one.")
                return
            elif len(inputs) > 1:
                await user.send("Too many options specified. You can only choose one.")
                return

            index = inputs[0] - 1
            numPlayers = len(game.Players)
            if not(index in range(0, numPlayers)):
                await user.send("You must pick one of the displayed options.")
                return

            if index in range(0, numPlayers - 1):
                otherPlayers = []
                for p in game.Players:
                    if p.Id == user.id:
                        continue
                    otherPlayers.append(p)

                newCard = otherPlayers[index].Card
                oPlayerName = otherPlayers[index].User.name
                cName = type(newCard).__name__
                oldCard = player.Card
                otherPlayers[index].Card = oldCard
                player.Card = newCard
                await user.send("You robbed %s.\nYour new role: %s" % \
                (oPlayerName,cName))
                player.CanDoNightAction = False
                player.HasDoneNightAction = True
            elif index in range(numPlayers - 1, numPlayers):
                await user.send("You have chosen to do nothing.")
                player.CanDoNightAction = False
                player.HasDoneNightAction = True
            else:
                await user.send("Weird input. Try again.")
        elif isinstance(player.InitialRole, Troublemaker) or \
        player.Card.Appearance == Troublemaker:
            if len(inputs) < 2:
                await user.send("You must enter two of the displayed options.")
                return
            elif len(inputs) > 2:
                await user.send("Too many options specified. You can only choose two.")
                return

            indices = []
            for input in inputs:
                indices.append(input - 1)
            numPlayers = len(game.Players)
            for index in indices:
                if not(index in range(0, numPlayers)):
                    await user.send("You must pick one of the displayed options.")
                    return

            otherPlayers = []
            for p in game.Players:
                if p.Id == user.id:
                    continue
                otherPlayers.append(p)

            cardA = otherPlayers[indices[0]].Card
            cardB = otherPlayers[indices[1]].Card
            playerNameA = otherPlayers[indices[0]].User.name
            playerNameB = otherPlayers[indices[1]].User.name
            otherPlayers[indices[0]].Card = cardB
            otherPlayers[indices[1]].Card = cardA
            await user.send("You swapped %s's and %s's roles." % \
            (playerNameA, playerNameB))
            player.CanDoNightAction = False
            player.HasDoneNightAction = True
        elif isinstance(player.InitialRole, Doppelganger):
            if len(inputs) < 1:
                await user.send("Too few options specified. You can only choose one.")
                return
            elif len(inputs) > 1:
                await user.send("Too many options specified. You can only choose one.")
                return

            index = inputs[0] - 1
            numPlayers = len(game.Players)
            if not(index in range(0, numPlayers)):
                await user.send("You must pick one of the displayed options.")
                return

            otherPlayers = []
            for p in game.Players:
                if p.Id == user.id:
                    continue
                otherPlayers.append(p)

            oPlayerName = otherPlayers[index].User.name
            cType = type(otherPlayers[index].Card)
            cName = cType.__name__
            player.Card.Appearance = cType
            await user.send(f"You mimic {oPlayerName}'s role: {cName}")
            if cType == Werewolf:
                player.Card.Team = Team.Werewolves
            elif cType == Minion:
                player.Card.Team = Team.Werewolves
                wwPlayers = game.GetPlayersFromInitRole(Werewolf)
                msg = "These are the other werewolves.\n\n"
                for i in range(0,numWWs):
                    wwUser = wwPlayers[i].User
                    if wwUser.id == player.User.id:
                        continue

                    if i == numWWs - 1:
                        msg = msg + wwUser.name
                    else:
                        msg = msg + wwUser.name + "\n"
                await user.send(msg)
            elif cType == Tanner:
                player.Card.Team = Team.Tanner
            player.CanDoNightAction = False
            player.HasDoneNightAction = True

    @commands.command()
    async def vote(self, ctx: commands.Context, vote: int):
        if not(ctx.channel.type is ChannelType.private):
            return

        db_gateway = self.settings.db_gateway
        dataTup = await SearchGameEntryByPlayerId(db_gateway, self.settings, \
        ctx.author.id)
        if dataTup is None:
            return
        game_master_id = dataTup[0]
        game = dataTup[1]

        if not(game.InVoting):
            return

        if not(GameHasStarted(game_master_id, self.settings)):
            return

        player = None
        for p in game.Players:
            if p.User.id == ctx.author.id:
                player = p
                break

        if player.HasVoted or not(player.CanVote):
            return

        vote -= 1
        user = player.User
        if not(vote in range(0, len(game.Players))):
            await user.send("You must pick one of the displayed options.")
            return

        voteTarget = game.Players[vote]
        if voteTarget.Id == user.id:
            await user.send("You can't vote for yourself.")
            return
        
        player.Vote = Vote(player, voteTarget)
        player.CanVote = False
        player.HasVoted = True
        await user.send(f"You voted for {voteTarget.User.name}.")

if __name__ == "__main__":
    pass
