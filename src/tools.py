def is_empty_list(list_):
    is_empty = False
    if len(list_) > 0:
        if type(list_[0]) is list:
            is_empty = is_empty_list(list_[0])
    else:
        is_empty = True
    return is_empty