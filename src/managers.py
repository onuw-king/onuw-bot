from ssl import SSLContext
from websockets import (
    WebSocketClientProtocol,
    connect
)
from asyncio import (
    sleep,
    gather
)
from json import (
    dumps,
    loads
)
from uuid import uuid4

from api_ops.db import (
    RequestType,
    Request
)

class TaskManager:
    def __init__(self):
        self.GameTasks = {}

    def AddGameTasks(self, tasks: list, gmId: int):
        if gmId in self.GameTasks:
            self.GameTasks[gmId].extend(tasks)
        else:
            self.GameTasks.update({gmId: tasks})

    def AbortGameTasks(self, gmId):
        if not(gmId in self.GameTasks):
            return
        tasks = self.GameTasks[gmId]
        for task in tasks:
            task.cancel()
        self.GameTasks.pop(gmId, None)

class DatabaseApiGateway:
    def __init__(self, host: str, port: str, nickname: str, \
    tls_ctx: SSLContext, self_token: str, db_token: str):
        self.url = f"wss://{host}:{port}/{uuid4().hex}/{nickname}/wss"
        self.tls_ctx = tls_ctx
        self.requests = dict()
        self.responses = dict()
        self.self_token = self_token
        self.db_token = db_token

    def add_request(self, request: Request, request_id: str):
        self.requests[request_id] = request

    async def get_response(self, request_id: str):
        while not(request_id in self.responses):
            await sleep(10/1000)
        response = self.responses[request_id]
        self.responses.pop(request_id, None)
        return response

    async def request_scheduler(self, ws: WebSocketClientProtocol):
        while True:
            if len(self.requests) > 0:
                reqs_to_clear = []
                for req_id, req in self.requests.items():
                    json_template = {
                        "api_token": self.self_token,
                        "request_id": req_id,
                        "request": req.dict()
                    }
                    json = dumps(json_template)
                    await ws.send(json.encode("utf-8"))
                    reqs_to_clear.append(req_id)
                for req_id in reqs_to_clear:
                    self.requests.pop(req_id, None)
            else:
                await sleep(50/1000)

    async def response_listener(self, ws: WebSocketClientProtocol):
        while True:
            raw_resp = await ws.recv()
            str_resp = raw_resp.decode("utf-8")
            resp = loads(str_resp)
            if not(type(resp) is dict):
                continue
            if not("api_token" in resp):
                continue
            if not("request_id" in resp):
                continue
            if not("response" in resp):
                continue
            if not(resp["api_token"] == self.db_token):
                continue
            self.responses[resp["request_id"]] = resp["response"]

    async def open(self, loop):
        async with connect(self.url, ssl=self.tls_ctx) as ws:
            tasks = [
                loop.create_task(self.request_scheduler(ws)),
                loop.create_task(self.response_listener(ws))
            ]
            await gather(*tasks)

class Settings:
    def __init__(self, logpath, logfolder, \
    db_api_gateway: DatabaseApiGateway):
        self.logfolder = logfolder
        self.logpath = logpath
        self.game_manifest = {}
        self.task_manager = TaskManager()
        self.db_api_gateway = db_api_gateway

if __name__ == "__main__":
    pass