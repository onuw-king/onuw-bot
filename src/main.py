from discord.ext import commands
from sys import argv
from os import (
    path,
    mkdir
)
from logging import (
    DEBUG,
    getLogger,
    FileHandler,
    Formatter,
    Logger
)
from ssl import (
    SSLContext,
    PROTOCOL_TLS_CLIENT
)

from managers import (
    DatabaseApiGateway,
    Settings
)
from cogs.game import (
    GeneralActions,
    ConfigActions,
    GameSetupActions,
    GameActions
)
from cogs.general import BackgroundProcesses

#ARGS:
#   1. client cert file name
#   2. client key file name
#   3. ca cert file name
#   4. db api host
#   5. db api port
#   6. client nickname
#   7. self api token
#   8. db api token
#   9. bot discord token

logFolder = "/app/logs/"
logPath = logFolder + "onuw_bot.log"
bot = commands.Bot(command_prefix='onuw.')
bot.remove_command('help')

tls_ctx = SSLContext(PROTOCOL_TLS_CLIENT)
tls_ctx.load_cert_chain(
    certfile=f"./certs/{argv[1]}",
    keyfile=f"./certs/{argv[2]}"
)
tls_ctx.load_verify_locations(f"./certs/{argv[3]}")
gateway = DatabaseApiGateway(str(argv[4]), str(argv[5]), str(argv[6]), tls_ctx, \
str(argv[7]), str(argv[8]))
settings = Settings(logPath, logFolder, gateway)

#start a logger to log stuff to a file
if not(path.exists(settings.logfolder)):
    mkdir(settings.logfolder)
logger = getLogger('discord')
logger.setLevel(DEBUG)
handler = FileHandler(filename=logPath, encoding='utf-8', mode='w')
handler.setFormatter(Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

bot.add_cog(BackgroundProcesses(bot, settings))
bot.add_cog(GeneralActions(bot, settings))
bot.add_cog(ConfigActions(bot, settings))
bot.add_cog(GameSetupActions(bot, settings))
bot.add_cog(GameActions(bot, settings))

bot.run(argv[9])
