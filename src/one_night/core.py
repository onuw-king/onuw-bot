from discord import *
from enum import Enum
from re import *
from random import shuffle

from one_night.cards import *

class CardConstraintType(Enum):
    MaxVillagers = 0
    MaxWerewolves = 1
    MaxSeers = 2
    MaxRobbers = 3
    MaxTroublemakers = 4
    MaxTanners = 5
    MaxDrunks = 6
    MaxHunters = 7
    MaxMasons = 8
    MaxInsomniacs = 9
    MaxMinions = 10
    MaxDoppelgangers = 11

class CardConstraint:
    def __init__(self, cardType, constType, val):
        self.CardType = cardType
        self.ConstraintType = constType
        self.Value = val

class GameConfig:
    def __init__(self, cardConfig: dict, hasLoneWolf: bool, gameDur: int, roleDur: int, \
    voteDur: int):
        self.CardConfig = cardConfig
        self.HasLoneWolf = hasLoneWolf
        self.DayPhaseDuration = gameDur
        self.VotingPhaseDuration = voteDur
        self.RoleDuration = roleDur
        self.KnownCards = {
            "villager": Villager,
            "werewolf": Werewolf,
            "seer": Seer,
            "robber": Robber,
            "troublemaker": Troublemaker,
            "tanner": Tanner,
            "drunk": Drunk,
            "hunter": Hunter,
            "mason": Mason,
            "insomniac": Insomniac,
            "minion": Minion,
            "doppelganger": Doppelganger
        }

class Game:
    MaxPlayers = 10
    MinPlayers = 3

    def __init__(self, players: list, config: GameConfig, playerCount: int):
        self.Players = players
        self.Config = config
        self.PlayerCount = playerCount
        self.HasStarted = False
        self.InDaytime = False
        self.InNighttime = False
        self.InDealPhase = False
        self.InVoting = False

        self.InitialRoles = {}
        self.MiddleCards = []
        self.Deck = []
        for cType, numCards in self.Config.CardConfig.items():
            cList = [self.Config.KnownCards[cType.name]()] * numCards
            self.Deck.extend(cList)

    def ShuffleDeck(self, numShuffles):
        for i in range(0,numShuffles):
            shuffle(self.Deck)

    def Deal(self):
        numPlayers = len(self.Players)
        self.MiddleCards = self.Deck[numPlayers:]
        for i in range(0,numPlayers):
            card = self.Deck[i]
            self.Players[i].Card = card
            self.Players[i].InitialRole = card
            self.InitialRoles[self.Players[i]] = card

    def GetPlayersFromCard(self, cardClass):
        players = []
        for player in self.Players:
            if isinstance(player.Card, cardClass):
                players.append(player)
        if players == []:
            return None
        else:
            return players

    def GetPlayersFromInitRole(self, cardClass):
        players = []
        for player, card in self.InitialRoles.items():
            if isinstance(card, cardClass):
                players.append(player)
        if players == []:
            return None
        else:
            return players

    def GetVotes(self):
        votes = []
        for player in self.Players:
            votes.append(player.Vote)
        return votes

class Player:
    def __init__(self, userid: int, user: User):
        self.Id = userid
        self.User = user
        self.Card = None
        self.CanDoNightAction = False
        self.HasDoneNightAction = False
        self.TaskIterations = 0
        self.Vote = None
        self.HasVoted = False
        self.CanVote = False
        self.IsAlive = True
        self.InitialRole = None

    def __eq__(self, other):
        if isinstance(other, Player):
            return self.Id == other.Id
        return False

    def __ne__(self, start):
        return not(self.__eq__(other))

    def __repr__(self):
        if self.User is None:
            return f"Player('', {self.Id})"
        return f"Player({self.User.name}, {self.Id})"

    def __hash__(self):
        return hash(self.__repr__())

class Vote:
    def __init__(self, votingPlayer, targetPlayer):
        self.Candidate = targetPlayer
        self.Voter = votingPlayer

def PlayerListToDict(players):
    pDict = {}
    for player in players:
        pDict.update(player.ToDictionary())
    return pDict

def PlayerDictToList(players):
    pList = []
    for pid, props in players.items():
        pList.append(Player(int(pid),props["Name"]))
    return pList

def HasTooManyOfCard(cardType, totalCards):
    constrs = GetCardConstraints()
    constr = constrs[cardType]
    if totalCards > constr.Value:
        return True
    return False

def GetCardConstraints():
    return {
        CardType.villager: CardConstraint(CardType.villager, CardConstraintType.MaxVillagers, 3),
        CardType.werewolf: CardConstraint(CardType.werewolf, CardConstraintType.MaxWerewolves, 2),
        CardType.seer: CardConstraint(CardType.seer, CardConstraintType.MaxSeers, 1),
        CardType.robber: CardConstraint(CardType.robber, CardConstraintType.MaxRobbers, 1),
        CardType.troublemaker: CardConstraint(CardType.troublemaker, CardConstraintType.MaxTroublemakers, 1),
        CardType.tanner: CardConstraint(CardType.tanner, CardConstraintType.MaxTanners, 1),
        CardType.drunk: CardConstraint(CardType.drunk, CardConstraintType.MaxDrunks, 1),
        CardType.hunter: CardConstraint(CardType.hunter, CardConstraintType.MaxHunters, 1),
        CardType.mason: CardConstraint(CardType.mason, CardConstraintType.MaxMasons, 2),
        CardType.insomniac: CardConstraint(CardType.insomniac, CardConstraintType.MaxInsomniacs, 1),
        CardType.minion: CardConstraint(CardType.minion, CardConstraintType.MaxMinions, 1),
        CardType.doppelganger: CardConstraint(CardType.doppelganger, CardConstraintType.MaxDoppelgangers, 1),
    }

def CardDictToPayload(cardDict):
    if cardDict == {}:
        return ""

    payload = ""
    for k, v in cardDict.items():
        payload += "%s:%d;" % (k.name, v)
    return payload[:-1]

def PayloadToCardDict(payload):
    if payload == "":
        return {}

    cDict = {}
    cardSets = payload.split(';')
    for cardSetInfo in cardSets:
        cardSet = cardSetInfo.split(':')
        numCards = int(cardSet[1])
        cardType = CardType[cardSet[0]]
        cDict[cardType] = numCards
    return cDict

def CalcPlayerCountFromCardPayload(payload):
    cardDict = PayloadToCardDict(payload)
    return CalcPlayerCountFromCardDict(cardDict)

def CalcPlayerCountFromCardDict(cardDict):
    sum = 0
    for _, v in cardDict.items():
        sum = sum + v
    return CalcPlayerCount(sum)

def CalcPlayerCount(numCards):
    return numCards - 3

def CalcCardCount(numPlayers):
    return numPlayers + 3

def IsValidCardPayload(payload):
    rePattern = r"([a-zA-Z]+:[0-9]+)((;([a-zA-Z]+:[0-9]+))+)?"
    if (fullmatch(rePattern, payload) is None):
        return False

    cDict = PayloadToCardDict(payload)
    cTypes = list(CardType)
    allAreValidKeys = False
    areValidKeys = []
    for k in cDict:
        areValidKeys.append(k in cTypes)

    numKeys = len(areValidKeys)
    for i in range(0,numKeys):
        if not(areValidKeys[i]):
            break
        elif i == numKeys - 1:
            allAreValidKeys = True
    return allAreValidKeys

if __name__ == "__main__":
    pass
