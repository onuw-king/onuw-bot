from enum import Enum
from random import randint

class CardType(Enum):
    #item number == index in the CardConfig table
    villager = 3
    werewolf = 4
    seer = 5
    robber = 6
    troublemaker = 7
    tanner = 8
    drunk = 9
    hunter = 10
    mason = 11
    insomniac = 12
    minion = 13
    doppelganger = 14

class Team(Enum):
    Villagers = 1
    Werewolves = 2
    Tanner = 3

class Card:
    def __init__(self, team, appearance: type):
        self.Team = team
        self.Appearance = appearance

    def DefaultNightAction(self):
        msg = "NightAction for %s is not implemented." % \
        type(self).__name__
        raise NotImplementedException(msg)

class Werewolf(Card):
    def __init__(self):
        super(Werewolf, self).__init__(Team.Werewolves, Werewolf)

class Villager(Card):
    def __init__(self):
        super(Villager, self).__init__(Team.Villagers, Villager)

class Seer(Card):
    def __init__(self):
        super(Seer, self).__init__(Team.Villagers, Seer)

class Troublemaker(Card):
    def __init__(self):
        super(Troublemaker, self).__init__(Team.Villagers, Troublemaker)

class Robber(Card):
    def __init__(self):
        super(Robber, self).__init__(Team.Villagers, Robber)

class Tanner(Card):
    def __init__(self):
        super(Tanner, self).__init__(Team.Tanner, Tanner)

class Minion(Card):
    def __init__(self):
        super(Minion, self).__init__(Team.Werewolves, Minion)

class Hunter(Card):
    def __init__(self):
        super(Hunter, self).__init__(Team.Villagers, Hunter)

class Drunk(Card):
    def __init__(self):
        super(Drunk, self).__init__(Team.Villagers, Drunk)

    def DefaultNightAction(self, player, midCards):
        numMCs = len(midCards)
        index = randint(0, numMCs + 1)
        oldCard = player.Card
        newCard = midCards[index]
        midCards[index] = oldCard
        player.Card = newCard

class Mason(Card):
    def __init__(self):
        super(Mason, self).__init__(Team.Villagers, Mason)

class Insomniac(Card):
    def __init__(self):
        super(Insomniac, self).__init__(Team.Villagers, Insomniac)

class Doppelganger(Card):
    def __init__(self):
        super(Doppelganger, self).__init__(Team.Villagers, Doppelganger)

if __name__ == "__main__":
    pass
