from enum import Enum

class Index(Enum):
    none = 0
    server_id = 1
    channel_id = 2
    role_id = 3
    game_master_id = 4
    user_id = 5
    author_id = 6
    game_config_name = 7
    player_count = 8

class DatabaseTable(Enum):
    admin_roles = 0
    active_games = 1
    active_players = 2
    active_game_configs = 3
    active_card_configs = 4
    game_configs = 5
    card_configs = 6

class DatabaseData:
    def dict(self):
        return vars(self)

    def __eq__(self, other):
        if not(isinstance(other, type(self))):
            return False

        self_dict = vars(self)
        other_dict = vars(other)
        if not(len(self_dict) == len(other_dict)):
            return False

        for exp_key, exp_val in self_dict.items():
            if not(other_dict[exp_key] == exp_val):
                return False
        return True
    
    def __ne__(self, other):
        return not(self.__eq__(other))
            
class AdminRole(DatabaseData):
    def __init__(self, **kwargs):
        self.server_id = kwargs["server_id"]
        self.role_id = kwargs["role_id"]

class ActiveGame(DatabaseData):
    def __init__(self, **kwargs):
        self.server_id = kwargs["server_id"]
        self.game_master_id = kwargs["game_master_id"]
        self.channel_id = kwargs["channel_id"]
        self.game_master_name = kwargs["game_master_name"]

class ActivePlayer(DatabaseData):
    def __init__(self, **kwargs):
        self.game_master_id = kwargs["game_master_id"]
        self.user_id = kwargs["user_id"]
        self.user_name = kwargs["user_name"]

class ActiveGameConfig(DatabaseData):
    def __init__(self, **kwargs):
        self.game_master_id = kwargs["game_master_id"]
        self.player_count = kwargs.get("player_count", -3)
        self.is_default_day_duration = kwargs.get("is_default_day_duration", True)
        self.has_lone_wolf_rule = kwargs.get("has_lone_wolf_rule", True)
        self.role_duration = kwargs.get("role_duration", 30)
        self.day_phase_duration = kwargs.get("day_phase_duration", -2)
        self.vote_phase_duration = kwargs.get("vote_phase_duration", 30)

class ActiveCardConfig(DatabaseData):
    def __init__(self, **kwargs):
        self.game_master_id = kwargs["game_master_id"]
        self.villager = kwargs.get("villager", 0)
        self.werewolf = kwargs.get("werewolf", 0)
        self.seer = kwargs.get("seer", 0)
        self.robber = kwargs.get("robber", 0)
        self.troublemaker = kwargs.get("troublemaker", 0)
        self.tanner = kwargs.get("tanner", 0)
        self.drunk = kwargs.get("drunk", 0)
        self.hunter = kwargs.get("hunter", 0)
        self.mason = kwargs.get("mason", 0)
        self.insomniac = kwargs.get("insomniac", 0)
        self.minion = kwargs.get("minion", 0)
        self.doppelganger = kwargs.get("doppelganger", 0)

class GameConfig(DatabaseData):
    def __init__(self, **kwargs):
        self.server_id = kwargs["server_id"]
        self.author_id = kwargs["author_id"]
        self.game_config_name = kwargs["game_config_name"]
        self.author_name = kwargs["author_name"]
        self.player_count = kwargs.get("player_count", -3)
        self.is_default_day_duration = kwargs.get("is_default_day_duration", True)
        self.has_lone_wolf_rule = kwargs.get("has_lone_wolf_rule", True)
        self.role_duration = kwargs.get("role_duration", 30)
        self.day_phase_duration = kwargs.get("day_phase_duration", -2)
        self.vote_phase_duration = kwargs.get("vote_phase_duration", 30)

class CardConfig(DatabaseData):
    def __init__(self, **kwargs):
        self.server_id = kwargs["server_id"]
        self.author_id = kwargs["author_id"]
        self.game_config_name = kwargs["game_config_name"]
        self.villager = kwargs.get("villager", 0)
        self.werewolf = kwargs.get("werewolf", 0)
        self.seer = kwargs.get("seer", 0)
        self.robber = kwargs.get("robber", 0)
        self.troublemaker = kwargs.get("troublemaker", 0)
        self.tanner = kwargs.get("tanner", 0)
        self.drunk = kwargs.get("drunk", 0)
        self.hunter = kwargs.get("hunter", 0)
        self.mason = kwargs.get("mason", 0)
        self.insomniac = kwargs.get("insomniac", 0)
        self.minion = kwargs.get("minion", 0)
        self.doppelganger = kwargs.get("doppelganger", 0)

def dbdata_factory(json_data: dict, table: DatabaseTable):
    data = None
    if table == DatabaseTable.admin_roles:
        data = AdminRole(**json_data)
    elif table == DatabaseTable.active_games:
        data = ActiveGame(**json_data)
    elif table == DatabaseTable.active_players:
        data = ActivePlayer(**json_data)
    elif table == DatabaseTable.active_game_configs:
        data = ActiveGameConfig(**json_data)
    elif table == DatabaseTable.active_card_configs:
        data = ActiveCardConfig(**json_data)
    elif table == DatabaseTable.game_configs:
        data = GameConfig(**json_data)
    elif table == DatabaseTable.card_configs:
        data = CardConfig(**json_data)
    else:
        raise ValueError(f"{table} is an unrecognized table.")
    return data